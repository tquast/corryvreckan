#include "HGCalTBNtupleReader.h"
#include <vector>

using namespace corryvreckan;
using namespace std;

HGCalTBNtupleReader::HGCalTBNtupleReader(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {
    m_filePath = m_config.get<std::string>("filePath", "tree.root");
    m_treeName = m_config.get<std::string>("treeName", "tree");
    m_HGADCLowerThreshold = m_config.get<float>("HGADCLowerThreshold", 20.);
    m_resolutionX = m_config.get<float>("resolutionX", 0.4);    //cm
    m_resolutionY = m_config.get<float>("resolutionY", 0.4);    //cm
    m_noisyChannelMask = m_config.get<int>("m_noisyChannelMask", -1);
    m_numberOfLayers = 0;
}

void HGCalTBNtupleReader::initialise() {

    LOG(DEBUG) << "Initialising HGCalTBNtupleReader";
    try {
        eventID = 0;
        readEvents = 0;
        m_inputFile = new TFile(m_filePath.c_str(), "READ");
        m_inputTree = (TTree*)m_inputFile->Get(m_treeName.c_str());
        std::cout << m_filePath.c_str() << std::endl;
        std::cout << m_treeName.c_str() << std::endl;
        // Create the input branches

        b_eventID = new TBranch;
        b_Nhits = new TBranch;
        b_rechit_layer_ = new TBranch;
        b_rechit_module_ = new TBranch;
        b_rechit_chip_ = new TBranch;
        b_rechit_channel_ = new TBranch;
        b_rechit_type_ = new TBranch;
        b_rechit_noise_flag_ = new TBranch;
        b_rechit_HGamp_ = new TBranch;
        b_rechit_LGamp_ = new TBranch;
        b_rechit_x_ = new TBranch;
        b_rechit_y_ = new TBranch;
        b_rechit_z_ = new TBranch;
        m_inputTree->SetBranchAddress("event", &eventID, &b_eventID);
        m_inputTree->SetBranchAddress("NRechits", &Nhits, &b_Nhits);
        m_inputTree->SetBranchAddress("rechit_layer", &rechit_layer_, &b_rechit_layer_);
        m_inputTree->SetBranchAddress("rechit_module", &rechit_module_, &b_rechit_module_);
        m_inputTree->SetBranchAddress("rechit_chip", &rechit_chip_, &b_rechit_chip_);
        m_inputTree->SetBranchAddress("rechit_channel", &rechit_channel_, &b_rechit_channel_);
        m_inputTree->SetBranchAddress("rechit_type", &rechit_type_, &b_rechit_type_);
        m_inputTree->SetBranchAddress("rechit_noise_flag", &rechit_noise_flag_, &b_rechit_noise_flag_);
        m_inputTree->SetBranchAddress("rechit_amplitudeHigh", &rechit_HGamp_, &b_rechit_HGamp_);
        m_inputTree->SetBranchAddress("rechit_amplitudeLow", &rechit_LGamp_, &b_rechit_LGamp_);
        m_inputTree->SetBranchAddress("rechit_x", &rechit_x_, &b_rechit_x_);
        m_inputTree->SetBranchAddress("rechit_y", &rechit_y_, &b_rechit_y_);
        m_inputTree->SetBranchAddress("rechit_z", &rechit_z_, &b_rechit_z_);

        rechit_layer_ = 0;
        rechit_module_ = 0;
        rechit_chip_ = 0;
        rechit_channel_ = 0;
        rechit_type_ = 0;
        rechit_noise_flag_ = 0;
        rechit_HGamp_ = 0;
        rechit_LGamp_ = 0;
        rechit_x_ = 0;
        rechit_y_ = 0;
        rechit_z_ = 0;

        LOG(DEBUG) << "Created tree: " << m_treeName;


        for (auto& detector : get_detectors()) m_numberOfLayers++;


    }
    catch (...) {
        throw ModuleError("Unable to read input file \"" + m_filePath + "\"");
    }

}

StatusCode HGCalTBNtupleReader::run(Clipboard* clipboard) {
    // Counter for cluster event ID
    unsigned int hit_layer, hit_type;
    unsigned int hit_module, hit_chip, hit_channel;
    bool is_noisy;
    Float16_t hit_hg_amp, hit_lg_amp, hit_x, hit_y, hit_z;

    m_inputTree->GetEntry(readEvents);

    std::vector<Clusters*> deviceData;
    std::vector<std::string> detectorIDs;
    for (unsigned int nlayer = 1; nlayer <= m_numberOfLayers; nlayer++) {
        std::string detectorID = "HGCalTB_layer" + std::to_string(nlayer);
        if (!has_detector(detectorID)) {
            LOG(DEBUG) << "Skipping unknown detector " << detectorID;
            continue;
        }
        deviceData.push_back(new Clusters);
        detectorIDs.push_back(detectorID);
    }

    for (unsigned int nhit = 0; nhit < Nhits; nhit++) {
        hit_hg_amp = rechit_HGamp_->at(nhit);
        if (hit_hg_amp < m_HGADCLowerThreshold) continue;
        hit_lg_amp = rechit_LGamp_->at(nhit);

        hit_layer = rechit_layer_->at(nhit);
        if (hit_layer > m_numberOfLayers) {
            LOG(DEBUG) << "Skipping unknown layer " << hit_layer;
            continue;
        }
        hit_module = rechit_module_->at(nhit);
        hit_chip = rechit_chip_->at(nhit);
        hit_channel = rechit_channel_->at(nhit);

        hit_type = rechit_type_->at(nhit);
        hit_x = rechit_x_->at(nhit) * Units::get("cm");
        hit_y = rechit_y_->at(nhit) * Units::get("cm");
        hit_z = rechit_z_->at(nhit) * Units::get("cm");
        is_noisy = rechit_noise_flag_->at(nhit);
        if (m_noisyChannelMask!=-1) {
            if (100*hit_chip+hit_channel==m_noisyChannelMask) is_noisy=true;
        }

        Cluster* cluster = new Cluster();
        cluster->setDetectorID("HGCalTB_layer" + std::to_string(hit_layer));
        // Create object with local cluster position
        PositionVector3D<Cartesian3D<double>> positionLocal(hit_x, hit_y, hit_z);
        cluster->setLayer(hit_layer);
        cluster->setModule(hit_module);
        cluster->setChip(hit_chip);
        cluster->setChannel(hit_channel);


        cluster->setClusterCentreLocal(hit_x, hit_y, 0);
        // Calculate global cluster position
        auto detector = get_detector(detectorIDs[hit_layer - 1]);
        PositionVector3D<Cartesian3D<double>> positionGlobal = detector->localToGlobal(positionLocal);
        cluster->setClusterCentre(positionGlobal);
        cluster->setNoisy(is_noisy);
        cluster->setPixelType(hit_type);
        cluster->setHGAmp(hit_hg_amp);
        cluster->setLGAmp(hit_lg_amp);
        cluster->setErrorX(m_resolutionX);
        cluster->setErrorY(m_resolutionY);

        deviceData[hit_layer - 1]->push_back(cluster);
    }

    for (unsigned int nlayer = 0; nlayer < m_numberOfLayers; nlayer++) {
        clipboard->put(detectorIDs[nlayer], "clusters", (Objects*)deviceData[nlayer]);
    }

    // Advance to next event if possible, otherwise end this run:
    readEvents++;
    if (readEvents < m_inputTree->GetEntries()) return Success;
    else {
        LOG(INFO) << "No more events in data stream.";
        return Failure;
    };

}

void HGCalTBNtupleReader::finalise() {
    LOG(DEBUG) << "Finalise";
    m_inputFile->Close();
}
