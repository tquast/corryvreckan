#ifndef HGCALTBNTUPLE_READER_H
#define HGCALTBNTUPLE_READER_H 1

#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Track.h"
#include "objects/Cluster.h"


namespace corryvreckan {
/** @ingroup Modules
 */
class HGCalTBNtupleReader : public Module {

public:
    // Constructors and destructors
    HGCalTBNtupleReader(Configuration config, std::vector<Detector*> detectors);
    ~HGCalTBNtupleReader() {}

    // Functions
    void initialise();
    StatusCode run(Clipboard* clipboard);
    void finalise();

    // Member variables
    unsigned int readEvents;

    unsigned int m_numberOfLayers;


    std::string m_filePath;
    std::string m_treeName;
    TFile* m_inputFile;
    TTree* m_inputTree;

    TBranch                   *b_eventID;
    TBranch                   *b_Nhits;
    TBranch                   *b_rechit_layer_;
    TBranch                   *b_rechit_module_;
    TBranch                   *b_rechit_chip_;
    TBranch                   *b_rechit_channel_;
    TBranch                   *b_rechit_type_;
    TBranch                   *b_rechit_noise_flag_;
    TBranch                   *b_rechit_HGamp_;
    TBranch                   *b_rechit_LGamp_;
    TBranch                   *b_rechit_x_;
    TBranch                   *b_rechit_y_;
    TBranch                   *b_rechit_z_;
      
    unsigned int eventID;
    unsigned int Nhits;
    std::vector<unsigned int>* rechit_layer_;
    std::vector<unsigned int>* rechit_module_;
    std::vector<unsigned int>* rechit_chip_;
    std::vector<unsigned int>* rechit_channel_;
    std::vector<unsigned int>* rechit_type_;
    std::vector<bool>* rechit_noise_flag_;
    std::vector<Float16_t>* rechit_HGamp_;
    std::vector<Float16_t>* rechit_LGamp_;
    std::vector<Float16_t>* rechit_x_;
    std::vector<Float16_t>* rechit_y_;
    std::vector<Float16_t>* rechit_z_;

    float m_HGADCLowerThreshold;
    float m_resolutionX;
    float m_resolutionY;

    int m_noisyChannelMask;
};
} // namespace corryvreckan
#endif // HGCALNTUPLE_READER_H
