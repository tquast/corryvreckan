#ifndef HGCALTBDATAOUTPUT_H
#define HGCALTBDATAOUTPUT_H 1

#include <iostream>
#include "Math/Point3D.h"
#include "Math/Vector3D.h"
#include "TFile.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Track.h"

namespace corryvreckan {
    /** @ingroup Modules
     */
    class HGCalTBDataOutput : public Module {

    public:
        // Constructors and destructors
        HGCalTBDataOutput(Configuration config, std::vector<Detector*> detectors);
        ~HGCalTBDataOutput() {}

        // Functions
        void initialise();
        StatusCode run(Clipboard* clipboard);
        void finalise();

        // Member variables
        int numPixels;
        int eventID;
        int filledEvents;

        int NTracks;
        std::vector<double> chi2;
        std::map<std::string, std::vector<double> > trackClusterX;
        std::map<std::string, std::vector<double> > trackClusterY;
        std::map<std::string, std::vector<double> > trackClusterZ;
        std::map<std::string, std::vector<double> > trackClusterAbsorber;
        
        std::map<std::string, Object*> m_objects;

        TTree* m_outputTree;

        std::string m_treeName;
    };
} // namespace corryvreckan
#endif // HGCalTBDataOutput_H
