#include "HGCalTBDataOutput.h"
#include <vector>

using namespace corryvreckan;
using namespace std;

HGCalTBDataOutput::HGCalTBDataOutput(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {

    m_treeName = m_config.get<std::string>("treeName", "tree");
}

void HGCalTBDataOutput::initialise() {
    LOG(DEBUG) << "Initialised HGCalTBDataOutput";

    m_outputTree = new TTree(m_treeName.c_str(), m_treeName.c_str());
    LOG(DEBUG) << "Created tree: " << m_treeName;

    eventID = 0;
    filledEvents = 0;
 
    // Create the output branches
    m_outputTree->Branch("EventID", &eventID);
    m_outputTree->Branch("NTracks", &NTracks);
    m_outputTree->Branch("chi2", &chi2);

    
    for(auto& detector : get_detectors()) {
        string detectorID = detector->name();

        trackClusterX[detectorID] = std::vector<double>(0.);
        trackClusterY[detectorID] = std::vector<double>(0.);
        trackClusterZ[detectorID] = std::vector<double>(0.);
        trackClusterAbsorber[detectorID] = std::vector<double>(0.);

        m_outputTree->Branch(("associatedClusterX_"+detectorID).c_str(), &trackClusterX[detectorID]);
        m_outputTree->Branch(("associatedClusterY_"+detectorID).c_str(), &trackClusterY[detectorID]);
        m_outputTree->Branch(("associatedClusterZ_"+detectorID).c_str(), &trackClusterZ[detectorID]);
        m_outputTree->Branch(("associatedAbsorber_"+detectorID).c_str(), &trackClusterAbsorber[detectorID]);
    }
}

StatusCode HGCalTBDataOutput::run(Clipboard* clipboard) {
    // Counter for cluster event ID
    eventID++;
    NTracks=0;

    // Getting tracks from the clipboard
    Tracks* tracks = (Tracks*)clipboard->get("tracks");
    if(tracks == NULL) {
        LOG(DEBUG) << "No tracks on the clipboard";
        m_outputTree->Fill();
        return Success;
    }

    NTracks = tracks->size();
    LOG(DEBUG)<<NTracks<<" tracks in event";
    // Iterate through tracks found
    
    chi2.clear();
    for (size_t i=0; i<NTracks; i++) chi2.push_back(-999);
    for(auto& detector : get_detectors()) {
        string detectorID = detector->name();
        trackClusterX[detectorID].clear();
        trackClusterY[detectorID].clear();
        trackClusterZ[detectorID].clear();
        trackClusterAbsorber[detectorID].clear();
        for (size_t i=0; i<NTracks; i++) {
            trackClusterX[detectorID].push_back(-999);
            trackClusterY[detectorID].push_back(-999);
            trackClusterZ[detectorID].push_back(-999);
            trackClusterAbsorber[detectorID].push_back(-999);
        }
        
    }
    int trackCounter=0;
    for(auto& track : (*tracks)) {
        LOG(DEBUG)<<"chi2: "<<track->chi2ndof();
        
        chi2[trackCounter] = track->chi2ndof();
        // CHeck if we have associated clusters:
        Clusters trackClusters = track->clusters();

        LOG(TRACE) << "Found track with associated cluster";

        for(auto& trackCluster : trackClusters) {
            string detectorID = trackCluster->detectorID();
            trackClusterX[detectorID][trackCounter] = trackCluster->globalX();
            trackClusterY[detectorID][trackCounter] = trackCluster->globalY();
            trackClusterZ[detectorID][trackCounter] = trackCluster->globalZ();
            trackClusterAbsorber[detectorID][trackCounter] = get_detector(detectorID)->getMaterialBudget();
            LOG(TRACE)<<"clusterX: "<<trackCluster->globalX();
            LOG(TRACE)<<"clusterX: "<<trackCluster->globalY();
            LOG(TRACE)<<"clusterZ: "<<trackCluster->globalZ();
            LOG(TRACE)<<"associatedAbsorber: "<<get_detector(detectorID)->getMaterialBudget();
        }

        trackCounter++;
    }
    m_outputTree->Fill();
    filledEvents++;
    if(filledEvents % 1000 == 0) {
        LOG(DEBUG) << "Events with single associated cluster: " << filledEvents;
    }

    // Return value telling analysis to keep running
    return Success;
}

void HGCalTBDataOutput::finalise() {
    LOG(DEBUG) << "Finalise";
}
