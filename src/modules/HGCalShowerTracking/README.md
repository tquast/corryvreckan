## Shower Tracking
**Maintainer**: Thorben Quast (<thorben.quast@cern.ch>)   
**Status**: Experimental 

#### Description
A MIP tracking algorithm for the 2018 HGCAL prototype detector based on iterative extensions of straight line trajectories through the layers. 


#### Requirements
* Reconstructed data from the 2018 HGCAL prototype in ntuple format.
* Converted hits into corryvreckan clusters using the HGCalTBNtupleReader.


#### Usage for the parasitic beam data taken in late October 2018
```toml
[HGCalShowerTracking]
typeSelection=true
noiseSelection=true
useLowGain=false
energyMaxThreshold=100000.
ringSumThreshold=5000.
min_HGAmp_MIP=25
max_HGAmp_MIP=200
NLayersForSeed=3
minClustersForSeed=2
maxChi2PerNdofForSeed=10
maxDistanceForAdding=1.5cm
maxNGaps=4
minNClustersForTrack=8
doubleCountingTolerance=2cm
prealignTranslationWithResiduals=false
```