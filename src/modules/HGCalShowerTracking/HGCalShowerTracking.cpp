#include "HGCalShowerTracking.h"
#include "TCanvas.h"
#include "objects/KDTree.h"
#include <chrono>

using namespace corryvreckan;
using namespace std;


bool compareTrackChi2NDof(Track* a, Track* b) { return (a->chi2ndof() < b->chi2ndof()); }
bool compareTrackNClusters(Track* a, Track* b) { return (a->nClusters() > b->nClusters()); }
bool compareDetectorByZPosition(Detector* a, Detector* b) { return (a->displacement().z() < b->displacement().z()); }

//recursive function which constructs all possible connections of clusters to form a track
void product(int detectorIndex, vector<string> &detectors, map<string, Clusters*> &clusters, map<string, Cluster*> cluster_memory, Tracks* track_hypotheses, int minDetectorIndex, int maxDetectorIndex, int minNClusters) {
    if (detectorIndex < maxDetectorIndex) {
        if (clusters[detectors[detectorIndex]]->size() == 0) {
            cluster_memory[detectors[detectorIndex]] = NULL;       //the cluster_memorys map is always overwritten
            product(detectorIndex + 1, detectors, clusters, cluster_memory, track_hypotheses, minDetectorIndex, maxDetectorIndex, minNClusters);
        } else {
            for (int clusterIndex = 0; clusterIndex < (int)clusters[detectors[detectorIndex]]->size(); clusterIndex++) {
                if (clusterIndex == -1) cluster_memory[detectors[detectorIndex]] = NULL;
                else cluster_memory[detectors[detectorIndex]] = clusters[detectors[detectorIndex]]->at(clusterIndex);       //the cluster_memorys map is always overwritten
                product(detectorIndex + 1, detectors, clusters, cluster_memory, track_hypotheses, minDetectorIndex, maxDetectorIndex, minNClusters);
            }
        }
    } else {
        Track* track = new Track();
        for (int d = minDetectorIndex; d < maxDetectorIndex; d++) if (cluster_memory[detectors[d]] != NULL) {
                track->addCluster(cluster_memory[detectors[d]]);
            }
        if (track->nClusters() >= minNClusters) {
            track->fit();    //at least minNClusters points for a fit, also calculates the chi2
            track_hypotheses->push_back(track);
        } else delete track;
    }
}

HGCalShowerTracking::HGCalShowerTracking(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {

    energyMaxThreshold = m_config.get<float>("energyMaxThreshold", 20000.);
    useLG = m_config.get<bool>("useLowGain", false);
    ringSumThreshold = m_config.get<float>("ringSumThreshold", 1400.);      //HG ADC in +/- 1.5cm around the center
    typeSelection = m_config.get<bool>("typeSelection", false);
    noiseSelection = m_config.get<bool>("noiseSelection", false);
    ignoreFirstNLayers = m_config.get<int>("ignoreFirstNLayers", 0);

    min_HGAmp_MIP = m_config.get<float>("min_HGAmp_MIP", 15.);
    max_HGAmp_MIP = m_config.get<float>("max_HGAmp_MIP", 200.);

    prealignTranslationWithResiduals = m_config.get<bool>("prealignTranslationWithResiduals", false);
    NLayersForSeed = m_config.get<int>("NLayersForSeed", 4);
    minClustersForSeed = m_config.get<int>("minClustersForSeed", 3);
    maxChi2PerNdofForSeed = m_config.get<float>("maxChi2PerNdofForSeed", 10.);
    maxDistanceForAdding = m_config.get<float>("maxDistanceForAdding", 3 * Units::get("cm"));
    maxNGaps = m_config.get<int>("maxNGaps", 4);
    minNClustersForTrack = m_config.get<int>("minNClustersForTrack", 12);
    m_DoubleCountingTolerance = m_config.get<float>("doubleCountingTolerance", 3.3 * Units::get("mm"));

    maxDistanceForAdding2  = pow(maxDistanceForAdding, 2);
    m_DoubleCountingTolerance2  = pow(m_DoubleCountingTolerance, 2);

}

void HGCalShowerTracking::initialise() {
    sorted_detector = get_detectors();
    std::sort(sorted_detector.begin(), sorted_detector.end(), compareDetectorByZPosition);

    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();
        detectors.push_back(detectorID);
        residuals_x[detectorID] = new TH1F(("residualx_" + detectorID).c_str(), ("residualx_" + detectorID).c_str(), 100, -2. * Units::get("cm"), 2. * Units::get("cm"));
        residuals_y[detectorID] = new TH1F(("residualy_" + detectorID).c_str(), ("residualy_" + detectorID).c_str(), 100, -2. * Units::get("cm"), 2. * Units::get("cm"));
    }

    std::string m_treeName = "HGCalTracking_Tracks";
    m_outputTrackTree = new TTree(m_treeName.c_str(), m_treeName.c_str());
    LOG(INFO) << "Created tree: " << m_treeName;
    m_outputTrackTree->Branch("eventID", &eventID);
    m_outputTrackTree->Branch("processingTime_mus", &processingTime_mus);
    m_outputTrackTree->Branch("track_NClusters", &track_NClusters);
    m_outputTrackTree->Branch("track_NAssociatedClusters", &track_NAssociatedClusters);
    m_outputTrackTree->Branch("track_chi2", &track_chi2);
    m_outputTrackTree->Branch("track_bx", &track_bx);
    m_outputTrackTree->Branch("track_by", &track_by);
    m_outputTrackTree->Branch("track_bz", &track_bz);
    m_outputTrackTree->Branch("track_mx", &track_mx);
    m_outputTrackTree->Branch("track_my", &track_my);
    m_outputTrackTree->Branch("track_mz", &track_mz);
    m_outputTrackTree->Branch("cluster_layer", &cluster_layer_);
    m_outputTrackTree->Branch("cluster_module", &cluster_module_);
    m_outputTrackTree->Branch("cluster_chip", &cluster_chip_);
    m_outputTrackTree->Branch("cluster_channel", &cluster_channel_);
    m_outputTrackTree->Branch("cluster_HGamp", &cluster_HGamp_);
    m_outputTrackTree->Branch("cluster_LGamp", &cluster_LGamp_);
    m_outputTrackTree->Branch("cluster_x", &cluster_x_);
    m_outputTrackTree->Branch("cluster_y", &cluster_y_);
    m_outputTrackTree->Branch("cluster_z", &cluster_z_);
    m_outputTrackTree->Branch("associated_cluster_layer", &associated_cluster_layer_);
    m_outputTrackTree->Branch("associated_cluster_module", &associated_cluster_module_);
    m_outputTrackTree->Branch("associated_cluster_chip", &associated_cluster_chip_);
    m_outputTrackTree->Branch("associated_cluster_channel", &associated_cluster_channel_);
    m_outputTrackTree->Branch("associated_cluster_HGamp", &associated_cluster_HGamp_);
    m_outputTrackTree->Branch("associated_cluster_LGamp", &associated_cluster_LGamp_);
    m_outputTrackTree->Branch("associated_cluster_x", &associated_cluster_x_);
    m_outputTrackTree->Branch("associated_cluster_y", &associated_cluster_y_);
    m_outputTrackTree->Branch("associated_cluster_z", &associated_cluster_z_);

    eventCounter = 0;
}

StatusCode HGCalShowerTracking::run(Clipboard* clipboard) {
    //LODEBUG) << "Start of event";
    eventCounter++;
    eventStartTime = std::chrono::steady_clock::now();

    eventID = eventCounter;

    /*******   Step 1: loading the clusters from the detectors    *******/
    map<string, Clusters*> selected_clusters_in_detector;

    unsigned int N_clusters_for_tracking = 0;
    float energySum = 0;
    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();
        // Get the clusters
        Clusters* tempClusters = (Clusters*)clipboard->get(detectorID, "clusters");
        selected_clusters_in_detector[detectorID] = new Clusters;

        for (size_t i = 0; i < tempClusters->size(); i++) {
            float HGEquivalent = useLG ? tempClusters->at(i)->getLGAmp() * 8 : tempClusters->at(i)->getHGAmp();
            energySum = energySum + HGEquivalent;
            if (HGEquivalent > max_HGAmp_MIP) continue;
            if (HGEquivalent < min_HGAmp_MIP) continue;
            if (noiseSelection && tempClusters->at(i)->getIsNoisy()) continue;
            if (typeSelection && tempClusters->at(i)->getPixeltype() != 0) continue;
            if (ignoreFirstNLayers >= tempClusters->at(i)->getLayer()) continue;

            float ringSum = 0;
            for (size_t j = 0; j < tempClusters->size(); j++) {
                if (i == j) continue;
                if (pow(tempClusters->at(i)->localX() - tempClusters->at(j)->localX(), 2) + pow(tempClusters->at(i)->localY() - tempClusters->at(j)->localY(), 2) < pow(1.5 * Units::get("cm"), 2)) ringSum += tempClusters->at(j)->localY();
            }
            if (ringSum > ringSumThreshold) continue;

            selected_clusters_in_detector[detectorID]->push_back(tempClusters->at(i));
            N_clusters_for_tracking++;
        }
        LOG(DEBUG) << "Loading " << selected_clusters_in_detector[detectorID]->size() <<  " clusters for" << detectorID;
    }
    LOG(DEBUG) << "Total energy in event " << energySum << "  with number of selected clusters: " << N_clusters_for_tracking;
    if (energySum > energyMaxThreshold) {
        for (auto& detector : get_detectors()) {
            delete selected_clusters_in_detector[detector->name()];
        }        
        return Success;
    }

    Tracks*  final_tracks = new Tracks;

    /*******   start from the first layer and iterate towards the last    *******/
    for (int d1 = 0; d1 < detectors.size() - NLayersForSeed; d1++) {     //
        Tracks* tracks_detector = new Tracks;

        /*******   Step 2a: start connecting the first n layers    *******/
        map<string, Cluster*> cluster_memory;
        Tracks* track_seeds_detector = new Tracks;
        /*
        for (auto& detector : get_detectors()) {
            LOG(DEBUG) << "BEGIN:  " << detector->name() << ": " << selected_clusters_in_detector[detector->name()]->size() << " clusters";
        }
        */
        product(d1, detectors, selected_clusters_in_detector, cluster_memory, track_seeds_detector, d1, d1 + NLayersForSeed, minClustersForSeed);     //4 detectors=layers, at least three clusters, todo: make configurable

        /*******   Step 2b: sorting and cutting based on chi2/ndof    *******/
        std::sort(track_seeds_detector->begin(), track_seeds_detector->end(), compareTrackChi2NDof);
        int NTracksAfterChi2Cut = track_seeds_detector->size();
        for (int i = NTracksAfterChi2Cut - 1; i > 0; --i) {
            if (track_seeds_detector->at(i)->chi2ndof() > maxChi2PerNdofForSeed) {NTracksAfterChi2Cut = i; delete track_seeds_detector->at(i);}
        }
        track_seeds_detector->resize(NTracksAfterChi2Cut);

        /*******   Step 3a: Extend every track as far as possible by adding clusters at every detector   *******/
        for (int d2 = d1 + NLayersForSeed; d2 < detectors.size(); d2++) {
            int NtracksForIteration = track_seeds_detector->size();
            if (NtracksForIteration == 0) break;

            for (int ntrack = NtracksForIteration - 1; ntrack >= 0; ntrack--) {
                Track* seed_track = track_seeds_detector->at(ntrack);
                bool delete_seed_track = false;

                for (size_t ncluster = 0; ncluster < selected_clusters_in_detector[detectors[d2]]->size(); ncluster++) {
                    Cluster* cluster = selected_clusters_in_detector[detectors[d2]]->at(ncluster);
                    if (seed_track->distance2(cluster) < maxDistanceForAdding2) {
                        Track* extendedTrack = new Track(seed_track);
                        extendedTrack->addCluster(cluster);
                        extendedTrack->fit();
                        /*******   Step 3b: cutting based on chi2/ndof    *******/
                        if (extendedTrack->chi2ndof() > maxChi2PerNdofForSeed) {
                            delete extendedTrack;
                        }
                        else {
                            LOG(DEBUG) << "Added cluster in layer " << detectors[d2] << ": " << cluster->getModule() * 1000 + cluster->getChip() * 100 + cluster->getChannel() << " to track: " << ntrack;
                            track_seeds_detector->push_back(extendedTrack);
                            delete_seed_track = true;
                        }
                    };
                }
                /*******   Step 3c: in case one cluster was successfully added, delete the original seed track    *******/
                if (delete_seed_track) {
                    //DEBUG: also delete the track stored in that position!
                    delete track_seeds_detector->at(ntrack);
                    track_seeds_detector->erase(track_seeds_detector->begin() + ntrack);
                }
            }
            /*******   Step 3d: sorting and cutting based number of missed layers    *******/
            std::sort(track_seeds_detector->begin(), track_seeds_detector->end(), compareTrackNClusters);
            int NTracksAfterGapCut = track_seeds_detector->size();
            int minNClustersBeforeCut = d2 - d1 - maxNGaps;
            for (int i = NTracksAfterGapCut - 1; i >= 0; --i) {
                if ((track_seeds_detector->at(i)->nClusters() <= minNClustersBeforeCut)) {
                    NTracksAfterGapCut = i;
                    /*******   Step 3e: add to track list in case number of clusters is sufficient    *******/
                    if (track_seeds_detector->at(i)->nClusters() >= minNClustersForTrack) {
                        tracks_detector->push_back(track_seeds_detector->at(i));
                    }
                    else delete track_seeds_detector->at(i);

                }
            }
            track_seeds_detector->resize(NTracksAfterGapCut);

            NTracksAfterGapCut = track_seeds_detector->size();
            /*******   Step 3f: removing clusters pointing to the same    *******/
            for (int i = NTracksAfterGapCut - 1; i >= 0; --i) {     //tracks are still ordered by number of clusters: fewest at the end
                for (int j = i - 1; j >= 0; --j) {
                    //check for overlap
                    if (track_seeds_detector->at(i)->beginLayer() > track_seeds_detector->at(j)->endLayer()) continue;
                    if (track_seeds_detector->at(i)->endLayer() < track_seeds_detector->at(j)->beginLayer()) continue;

                    ROOT::Math::XYZPoint X1 = track_seeds_detector->at(i)->intercept(sorted_detector[d2]->displacement().Z());
                    ROOT::Math::XYZPoint X2 = track_seeds_detector->at(j)->intercept(sorted_detector[d2]->displacement().Z());
                    if (pow(X1.X() - X2.X(), 2) + pow(X1.Y() - X2.Y(), 2) >= m_DoubleCountingTolerance2) continue;      //10 mm2 tolerance
                    LOG(DEBUG) << "Removing track " << i << ": " << pow(X1.X() - X2.X(), 2) + pow(X1.Y() - X2.Y(), 2) << " < " << m_DoubleCountingTolerance2;
                    LOG(DEBUG) << track_seeds_detector->at(i)->beginLayer() << "-" << track_seeds_detector->at(i)->endLayer();
                    LOG(DEBUG) << track_seeds_detector->at(j)->beginLayer() << "-" << track_seeds_detector->at(j)->endLayer();
                    delete track_seeds_detector->at(i);
                    track_seeds_detector->erase(track_seeds_detector->begin() + i);
                    break;
                }
            }

            /*******   Step 3g: if the last detector is reached, add the tracks    *******/
            if (d2 == detectors.size() - 1) {
                for (int i = track_seeds_detector->size() - 1; i >= 0; --i) {
                    if (track_seeds_detector->at(i)->nClusters() >= minNClustersForTrack) {
                        LOG(DEBUG) << "Track " << i << " with " << track_seeds_detector->at(i)->nClusters() << " clusters to be added";
                        tracks_detector->push_back(new Track(track_seeds_detector->at(i)));
                    }
                    delete track_seeds_detector->at(i);
                }
            }
        }
        track_seeds_detector->clear();
        delete track_seeds_detector;


        LOG(DEBUG) << "Detector " << d1 << " with " << tracks_detector->size() << " tracks";
        for (size_t ntrack = 0; ntrack < tracks_detector->size(); ntrack++) {
            LOG(DEBUG) << "Track " << ntrack << " with " << tracks_detector->at(ntrack)->nClusters() << " clusters";
            /*******   Step 4: Remove clusters which are already taken by the tracks to prevent double counting    *******/
            for (size_t ncluster = 0; ncluster < tracks_detector->at(ntrack)->nClusters(); ncluster++) {
                Cluster* clusterForRemovalFromList = tracks_detector->at(ntrack)->clusters()[ncluster];

                auto it = selected_clusters_in_detector[clusterForRemovalFromList->detectorID()]->begin();
                LOG(DEBUG) << clusterForRemovalFromList->detectorID() << ":  " << clusterForRemovalFromList->getModule()<<"  "<<clusterForRemovalFromList->getChip()<<"  "<<clusterForRemovalFromList->getChannel();

                for (it; it != selected_clusters_in_detector[clusterForRemovalFromList->detectorID()]->end(); it++) {
                    //LOG(DEBUG) << "Trying " << (*it)->detectorID() << "   " << (*it)->getModule() * 1000 + (*it)->getChip() * 100 + (*it)->getChannel() << std::endl;
                    if (clusterForRemovalFromList->getModule() * 1000 + clusterForRemovalFromList->getChip() * 100 + clusterForRemovalFromList->getChannel() != (*it)->getModule() * 1000 + (*it)->getChip() * 100 + (*it)->getChannel()) continue;
                    //LOG(DEBUG) << "Erasing cluster: " << clusterForRemovalFromList->getModule() * 1000 + clusterForRemovalFromList->getChip() * 100 + clusterForRemovalFromList->getChannel();
                    selected_clusters_in_detector[clusterForRemovalFromList->detectorID()]->erase(it);
                    break;      //per definition only one cluster per detector added to the track

                }
                //std::cout<<selected_clusters_in_detector[clusterForRemovalFromList->detectorID()]->size()<<std::endl;
            }
            /*******   Step 5: Add the track    *******/
            final_tracks->push_back(new Track(tracks_detector->at(ntrack)));
            delete tracks_detector->at(ntrack);

        }
        /*
        for (auto& detector : get_detectors()) {
            LOG(DEBUG) << "END " << detector->name() << ": " << selected_clusters_in_detector[detector->name()]->size() << " clusters";
        }
        */

        //DEBUG, 18 March 2019: must delete tracks_detector entries!
        tracks_detector->clear();
        delete tracks_detector;
    }

    /*******   Step 6: Write out tracks & clusters   *******/
    processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
    for (size_t ntrack = 0; ntrack < final_tracks->size(); ntrack++) {

        /*******   Step 6a: Filling residual histograms   *******/
        Clusters trackClusters = final_tracks->at(ntrack)->clusters();
        for (auto& trackCluster : trackClusters) {
            ROOT::Math::XYZPoint intercept = final_tracks->at(ntrack)->intercept(trackCluster->globalZ());
            residuals_x[trackCluster->detectorID()]->Fill(intercept.X() - trackCluster->globalX());
            residuals_y[trackCluster->detectorID()]->Fill(intercept.Y() - trackCluster->globalY());
        }

        /*******   Step 6b: Assigning associated clusters without any energy requirement   *******/
        for (auto& detector : get_detectors()) {
            string detectorID = detector->name();
            // Get the clusters
            Clusters* tempClusters = (Clusters*)clipboard->get(detectorID, "clusters");
            for (size_t i = 0; i < tempClusters->size(); i++) {
                if (final_tracks->at(ntrack)->distance2(tempClusters->at(i)) > maxDistanceForAdding2) continue;
                if (tempClusters->at(i)->getLayer() > final_tracks->at(ntrack)->endLayer()) continue;
                if (tempClusters->at(i)->getLayer() < final_tracks->at(ntrack)->beginLayer()) continue;
                final_tracks->at(ntrack)->addAssociatedCluster(tempClusters->at(i));
            }

        }


        /*******   Step 6c: Writing to the ntuple   *******/
        track_chi2 = final_tracks->at(ntrack)->chi2ndof();
        track_bx = final_tracks->at(ntrack)->m_state.X() / Units::get("cm");
        track_by = final_tracks->at(ntrack)->m_state.Y() / Units::get("cm");
        track_bz = final_tracks->at(ntrack)->m_state.Z() / Units::get("cm");
        track_mx = final_tracks->at(ntrack)->m_direction.X();
        track_my = final_tracks->at(ntrack)->m_direction.Y();
        track_mz = final_tracks->at(ntrack)->m_direction.Z();
        track_NClusters = (int)final_tracks->at(ntrack)->clusters().size();
        track_NAssociatedClusters = (int)final_tracks->at(ntrack)->associatedClusters().size();
        cluster_layer_.clear();
        cluster_module_.clear();
        cluster_chip_.clear();
        cluster_channel_.clear();
        cluster_HGamp_.clear();
        cluster_LGamp_.clear();
        cluster_x_.clear();
        cluster_y_.clear();
        cluster_z_.clear();
        associated_cluster_layer_.clear();
        associated_cluster_module_.clear();
        associated_cluster_chip_.clear();
        associated_cluster_channel_.clear();
        associated_cluster_HGamp_.clear();
        associated_cluster_LGamp_.clear();
        associated_cluster_x_.clear();
        associated_cluster_y_.clear();
        associated_cluster_z_.clear();


        for (auto& trackCluster : trackClusters) {
            cluster_layer_.push_back(trackCluster->getLayer());
            cluster_module_.push_back(trackCluster->getModule());
            cluster_chip_.push_back(trackCluster->getChip());
            cluster_channel_.push_back(trackCluster->getChannel());
            cluster_HGamp_.push_back(trackCluster->getHGAmp());
            cluster_LGamp_.push_back(trackCluster->getLGAmp());
            cluster_x_.push_back(trackCluster->localX() / Units::get("cm"));
            cluster_y_.push_back(trackCluster->localY() / Units::get("cm"));
            cluster_z_.push_back(trackCluster->globalZ() / Units::get("cm"));
        }
        
        Clusters associatedTrackClusters = final_tracks->at(ntrack)->associatedClusters();
        for (auto& associatedTrackCluster : associatedTrackClusters) {
            associated_cluster_layer_.push_back(associatedTrackCluster->getLayer());
            associated_cluster_module_.push_back(associatedTrackCluster->getModule());
            associated_cluster_chip_.push_back(associatedTrackCluster->getChip());
            associated_cluster_channel_.push_back(associatedTrackCluster->getChannel());
            associated_cluster_HGamp_.push_back(associatedTrackCluster->getHGAmp());
            associated_cluster_LGamp_.push_back(associatedTrackCluster->getLGAmp());
            associated_cluster_x_.push_back(associatedTrackCluster->localX() / Units::get("cm"));
            associated_cluster_y_.push_back(associatedTrackCluster->localY() / Units::get("cm"));
            associated_cluster_z_.push_back(associatedTrackCluster->globalZ() / Units::get("cm"));
        }

        m_outputTrackTree->Fill();
    }


    LOG(DEBUG) << "End of event: " << final_tracks->size() << " tracks constructed" << std::endl << std::endl << std::endl;
    clipboard->put("tracks", (Objects*)final_tracks);
    //delete final_tracks;
    for (auto& detector : get_detectors()) {
        delete selected_clusters_in_detector[detector->name()];
    }
    return Success;
}

void HGCalShowerTracking::finalise() {
    if (prealignTranslationWithResiduals) {
        LOG(INFO) << "Performing prealignment based on full track residuals";
        //determine mean residuals
        TF1* gaus = new TF1("gaus", "gaus");

        for (int d = 0; d < detectors.size(); d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);

            double mean_x = residuals_x[detector_name]->GetXaxis()->GetBinCenter(residuals_x[detector_name]->GetMaximumBin());
            double rms_x = residuals_x[detector_name]->GetRMS();
            double mean_y = residuals_y[detector_name]->GetXaxis()->GetBinCenter(residuals_y[detector_name]->GetMaximumBin());
            double rms_y = residuals_y[detector_name]->GetRMS();
            gaus->SetParameter(0, residuals_x[detector_name]->GetMaximum());
            gaus->SetParameter(1, mean_x);
            gaus->SetParameter(2, rms_x);
            gaus->SetRange(mean_x - 2.0 * rms_x, mean_x + 2.0 * rms_x);
            residuals_x[detector_name]->Fit(gaus, "RQ");
            double displacementX = gaus->GetParameter(1);
            gaus->SetParameter(0, residuals_y[detector_name]->GetMaximum());
            gaus->SetParameter(1, mean_y);
            gaus->SetParameter(2, rms_y);
            gaus->SetRange(mean_y - 2.0 * rms_y, mean_y + 2.0 * rms_y);
            residuals_y[detector_name]->Fit(gaus, "RQ");
            double displacementY = gaus->GetParameter(1);
            LOG(INFO) << "Detector: " << detector_name;
            LOG(INFO) << "displacementX: " << displacementX;
            LOG(INFO) << "displacementY: " << displacementY;

            double x0 = detector->displacement().X();
            double y0 = detector->displacement().Y();
            detector->displacementX(x0 + displacementX);
            detector->displacementY(y0 + displacementY);
        }
        delete gaus;
    }
}