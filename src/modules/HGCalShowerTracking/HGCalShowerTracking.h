#ifndef HGCALSHOWERTRACKING_H
#define HGCALSHOWERTRACKING_H 1

#include <iostream>
#include <algorithm>
#include "TCanvas.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TF1.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Cluster.h"
#include "objects/Pixel.h"
#include "objects/Track.h"

namespace corryvreckan {
/** @ingroup Modules
 */
class HGCalShowerTracking : public Module {

public:
    // Constructors and destructors
    HGCalShowerTracking(Configuration config, std::vector<Detector*> detectors);
    ~HGCalShowerTracking() {}

    // Functions
    void initialise();
    StatusCode run(Clipboard* clipboard);
    void finalise();

private:
    std::vector<Detector*> sorted_detector;

    int eventCounter;

    std::vector<std::string> detectors;

    //configuration
    float energyMaxThreshold;
    bool typeSelection;
    bool noiseSelection;
    bool useLG;
    float ringSumThreshold;
    int ignoreFirstNLayers;

    float min_HGAmp_MIP;
    float max_HGAmp_MIP;

    int NLayersForSeed;
    int minClustersForSeed;
    float maxChi2PerNdofForSeed;
    float maxDistanceForAdding;
    float maxDistanceForAdding2;
    int maxNGaps;
    int minNClustersForTrack;

    float m_DoubleCountingTolerance;
    float m_DoubleCountingTolerance2;

    bool prealignTranslationWithResiduals;
    std::map<std::string, TH1F*> residuals_x;
    std::map<std::string, TH1F*> residuals_y;



    //Book keeping
    TH2F* hist_x;
    TH2F* hist_y;
    TH2F* hist_offset;
    TH2F* hist_offset_occupancy;
    TH2F* hist_slope;
    TH2F* hist_slope_occupancy;


    TTree* m_outputTrackTree;
    unsigned int eventID;
    int track_NClusters;
    int track_NAssociatedClusters;
    float track_chi2;
    float track_bx;
    float track_by;
    float track_bz;
    float track_mx;
    float track_my;
    float track_mz;
    std::vector<unsigned int> cluster_layer_;
    std::vector<unsigned int> cluster_module_;
    std::vector<unsigned int> cluster_chip_;
    std::vector<unsigned int> cluster_channel_;
    std::vector<Float16_t> cluster_HGamp_;
    std::vector<Float16_t> cluster_LGamp_;
    std::vector<Float16_t> cluster_x_;
    std::vector<Float16_t> cluster_y_;
    std::vector<Float16_t> cluster_z_;
    std::vector<unsigned int> associated_cluster_layer_;
    std::vector<unsigned int> associated_cluster_module_;
    std::vector<unsigned int> associated_cluster_chip_;
    std::vector<unsigned int> associated_cluster_channel_;
    std::vector<Float16_t> associated_cluster_HGamp_;
    std::vector<Float16_t> associated_cluster_LGamp_;
    std::vector<Float16_t> associated_cluster_x_;
    std::vector<Float16_t> associated_cluster_y_;
    std::vector<Float16_t> associated_cluster_z_;

    std::chrono::steady_clock::time_point eventStartTime;
    unsigned int processingTime_mus;
};

} // namespace corryvreckan
#endif // HGCALShowerTRACKING_H
