#ifndef HGCALSIMNTUPLE_READER_H
#define HGCALSIMNTUPLE_READER_H 1

#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Track.h"
#include "objects/Cluster.h"
#include "TRandom3.h"


struct noise_hit {
    float x;
    float y;
    float z;
    int module;
    int layer;
    int chip;
    int channel;
};


namespace corryvreckan {
/** @ingroup Modules
 */
class HGCalSimNtupleReader : public Module {

public:
    // Constructors and destructors
    HGCalSimNtupleReader(Configuration config, std::vector<Detector*> detectors);
    ~HGCalSimNtupleReader() {}

    // Functions
    void initialise();
    StatusCode run(Clipboard* clipboard);
    void finalise();

    // Member variables
    unsigned int readEvents;

    unsigned int m_numberOfLayers;


    std::string m_filePath;
    std::string m_treeName;
    TFile* m_inputFile;
    TTree* m_inputTree;

    TBranch* b_eventID;
    TBranch* b_simhit_ID_;
    TBranch* b_simhit_energy_;
    TBranch* b_simhit_x_;
    TBranch* b_simhit_y_;
    TBranch* b_simhit_z_;
      
    int eventID;

    std::vector<float>* simhit_energy_;
    std::vector<int>* simhit_ID_;
    std::vector<float>* simhit_x_;
    std::vector<float>* simhit_y_;
    std::vector<float>* simhit_z_;


    float m_resolutionX;
    float m_resolutionY;

    int m_configID;
    int m_NModulesInEE;
    int m_NModules;
    float m_keV_to_HG_ADC;
    float m_HG_ADC_to_LG_ADC;

    int N_noisyChannelList;
    std::map<int, noise_hit*> noise_hits_map;
    TRandom* randgen;
    float m_mean_energyNoise;
    float m_width_energyNoise;
    int m_NNoiseHits;
    int m_NNoiseHitsSpread;

};
} // namespace corryvreckan
#endif // HGCALNTUPLE_READER_H
