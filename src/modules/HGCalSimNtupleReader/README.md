## HGCalTBDataOutput
**Maintainer**: Thorben Quast (<thorben.quast@cern.ch>)   
**Status**: Development   


#### Description




#### Parameters
* `name`: <description>







#### Plots produced
* Track chi^2 histogram


#### Usage
```toml
[HGCalTBNtupleReader]
fileName = "myInputFile.root"
treeName = "myTree"

```
