#include "HGCalSimNtupleReader.h"
#include <vector>

using namespace corryvreckan;
using namespace std;

HGCalSimNtupleReader::HGCalSimNtupleReader(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {
    m_filePath = m_config.get<std::string>("filePath", "tree.root");
    m_treeName = m_config.get<std::string>("treeName", "CaloHits");
    m_resolutionX = m_config.get<float>("resolutionX", 0.4);    //cm
    m_resolutionY = m_config.get<float>("resolutionY", 0.4);    //cm
    m_configID = m_config.get<int>("configurationID", 25);
    m_keV_to_HG_ADC = m_config.get<float>("keVToHGADC", 1. / 85 * 40);
    m_HG_ADC_to_LG_ADC = m_config.get<float>("HGADCToLGADC", 1. / 8.);
    m_mean_energyNoise = m_config.get<float>("mean_energyNoise", 15.);      //HG ADC
    m_width_energyNoise = m_config.get<float>("width_energyNoise", 8.);      //HG ADC
    m_NNoiseHits = m_config.get<int>("NNoiseHits", 100.);
    m_NNoiseHitsSpread = m_config.get<int>("NNoiseHitsSpread", 20.);

    m_numberOfLayers = 0;

    switch (m_configID) {
    case 22:
        m_NModulesInEE = 28;
        m_NModules = 66;
        break;
    case 25:
        m_NModulesInEE = 7;
        m_NModules = 91;
        break;
    default:
        m_NModulesInEE = 28;
        m_NModules = 94;
    }
}

void HGCalSimNtupleReader::initialise() {

    LOG(DEBUG) << "Initialising HGCalSimNtupleReader";
    try {
        eventID = 0;
        readEvents = 0;
        m_inputFile = new TFile(m_filePath.c_str(), "READ");
        m_inputTree = (TTree*)m_inputFile->Get(m_treeName.c_str());
        std::cout << m_filePath.c_str() << std::endl;
        std::cout << m_treeName.c_str() << std::endl;
        // Create the input branches

        b_eventID = new TBranch;
        b_simhit_ID_ = new TBranch;
        b_simhit_energy_ = new TBranch;
        b_simhit_x_ = new TBranch;
        b_simhit_y_ = new TBranch;
        b_simhit_z_ = new TBranch;
        m_inputTree->SetBranchAddress("eventID", &eventID, &b_eventID);
        m_inputTree->SetBranchAddress("ID", &simhit_ID_, &b_simhit_ID_);
        m_inputTree->SetBranchAddress("Edep_keV", &simhit_energy_, &b_simhit_energy_);
        m_inputTree->SetBranchAddress("x_cm", &simhit_x_, &b_simhit_x_);
        m_inputTree->SetBranchAddress("y_cm", &simhit_y_, &b_simhit_y_);
        m_inputTree->SetBranchAddress("z_cm", &simhit_z_, &b_simhit_z_);

        simhit_energy_ = 0;
        simhit_ID_ = 0;
        simhit_x_ = 0;
        simhit_y_ = 0;
        simhit_z_ = 0;

        LOG(DEBUG) << "Created tree: " << m_treeName;

        for (auto& detector : get_detectors()) m_numberOfLayers++;

        int hit_id;
        int hit_layer, hit_module, hit_chip, hit_channel;
        float hit_x, hit_y, hit_z;
        for (int i = 0; i < m_inputTree->GetEntries(); i++) {
            m_inputTree->GetEntry(i);
            for (unsigned int nhit = 0; nhit < simhit_ID_->size(); nhit++) {
                hit_id = simhit_ID_->at(nhit);
                if (noise_hits_map.find(hit_id) != noise_hits_map.end()) continue;

                noise_hits_map[hit_id] = new noise_hit;

                hit_module = hit_id / 1000 + 1;
                hit_chip = (hit_id % 1000) / 64;
                hit_channel = hit_id % 64;

                if (hit_module <= m_NModulesInEE)
                    hit_layer = hit_module;
                else
                    hit_layer = (hit_module - m_NModulesInEE) / 7 + m_NModulesInEE;

                hit_x = simhit_x_->at(nhit) * Units::get("cm");
                hit_y = simhit_y_->at(nhit) * Units::get("cm");
                hit_z = simhit_z_->at(nhit) * Units::get("cm");

                noise_hits_map[hit_id]->x = hit_x;
                noise_hits_map[hit_id]->y = hit_y;
                noise_hits_map[hit_id]->z = hit_z;
                noise_hits_map[hit_id]->layer = hit_layer;
                noise_hits_map[hit_id]->module = hit_module;
                noise_hits_map[hit_id]->chip = hit_chip;
                noise_hits_map[hit_id]->channel = hit_channel;
            }
        }
        N_noisyChannelList = noise_hits_map.size();
        randgen = new TRandom();
    }
    catch (...) {
        throw ModuleError("Unable to read input file \"" + m_filePath + "\"");
    }

}

StatusCode HGCalSimNtupleReader::run(Clipboard* clipboard) {
    // Counter for cluster event ID
    int hit_id;
    unsigned int hit_layer, hit_type;
    unsigned int hit_module, hit_chip, hit_channel;
    bool is_noisy;
    float hit_hg_amp, hit_lg_amp, hit_x, hit_y, hit_z;

    m_inputTree->GetEntry(readEvents);

    std::vector<Clusters*> deviceData;
    std::vector<std::string> detectorIDs;
    for (unsigned int nlayer = 1; nlayer <= m_numberOfLayers; nlayer++) {
        std::string detectorID = "HGCalTB_layer" + std::to_string(nlayer);
        if (!has_detector(detectorID)) {
            LOG(DEBUG) << "Skipping unknown detector " << detectorID;
            continue;
        }
        deviceData.push_back(new Clusters);
        detectorIDs.push_back(detectorID);
    }

    for (unsigned int nhit = 0; nhit < simhit_ID_->size(); nhit++) {
        hit_id = simhit_ID_->at(nhit);
        hit_module = hit_id / 1000 + 1;
        hit_chip = (hit_id % 1000) / 64;
        hit_channel = hit_id % 64;

        hit_hg_amp = simhit_energy_->at(nhit) * m_keV_to_HG_ADC;
        hit_lg_amp = hit_hg_amp * m_HG_ADC_to_LG_ADC;

        if (hit_module <= m_NModulesInEE)
            hit_layer = hit_module;
        else
            hit_layer = (hit_module - m_NModulesInEE) / 7 + m_NModulesInEE;

        if (hit_layer > m_numberOfLayers) {
            LOG(WARNING) << "Skipping unknown layer " << hit_layer;
            continue;
        }

        hit_type = 0;
        hit_x = simhit_x_->at(nhit) * Units::get("cm");
        hit_y = simhit_y_->at(nhit) * Units::get("cm");
        hit_z = simhit_z_->at(nhit) * Units::get("cm");
        is_noisy = false;


        Cluster* cluster = new Cluster();
        cluster->setDetectorID("HGCalTB_layer" + std::to_string(hit_layer));
        // Create object with local cluster position
        PositionVector3D<Cartesian3D<double>> positionLocal(hit_x, hit_y, hit_z);
        cluster->setLayer(hit_layer);
        cluster->setModule(hit_module);
        cluster->setChip(hit_chip);
        cluster->setChannel(hit_channel);


        cluster->setClusterCentreLocal(hit_x, hit_y, 0);
        // Calculate global cluster position
        auto detector = get_detector(detectorIDs[hit_layer - 1]);
        PositionVector3D<Cartesian3D<double>> positionGlobal = detector->localToGlobal(positionLocal);
        cluster->setClusterCentre(positionGlobal);
        cluster->setNoisy(is_noisy);
        cluster->setPixelType(hit_type);
        cluster->setHGAmp(hit_hg_amp);
        cluster->setLGAmp(hit_lg_amp);
        cluster->setErrorX(m_resolutionX);
        cluster->setErrorY(m_resolutionY);

        deviceData[hit_layer - 1]->push_back(cluster);
    }

    //add noisy hits:
    m_mean_energyNoise = m_config.get<float>("mean_energyNoise", 15.);      //HG ADC
    m_width_energyNoise = m_config.get<float>("width_energyNoise", 8.);      //HG ADC

    int NNoisyHits = randgen->Gaus(m_NNoiseHits, m_NNoiseHitsSpread);
    if (NNoisyHits > 0) {
        for (auto noiseHitCandidate : noise_hits_map) {
            if (randgen->Uniform(0, 1) > 1.*NNoisyHits / N_noisyChannelList) continue;

            float noise_hit_energy = randgen->Gaus(m_mean_energyNoise, m_width_energyNoise);

            Cluster* cluster = new Cluster();
            cluster->setDetectorID("HGCalTB_layer" + std::to_string(noiseHitCandidate.second->layer));
            // Create object with local cluster position
            PositionVector3D<Cartesian3D<double>> positionLocal(noiseHitCandidate.second->x, noiseHitCandidate.second->y, noiseHitCandidate.second->z);
            cluster->setLayer(noiseHitCandidate.second->layer);
            cluster->setModule(noiseHitCandidate.second->module);
            cluster->setChip(noiseHitCandidate.second->chip);
            cluster->setChannel(noiseHitCandidate.second->channel);


            cluster->setClusterCentreLocal(noiseHitCandidate.second->x, noiseHitCandidate.second->y, 0);
            // Calculate global cluster position
            auto detector = get_detector(detectorIDs[noiseHitCandidate.second->layer - 1]);
            PositionVector3D<Cartesian3D<double>> positionGlobal = detector->localToGlobal(positionLocal);
            cluster->setClusterCentre(positionGlobal);
            cluster->setNoisy(false);
            cluster->setPixelType(0);
            cluster->setHGAmp(noise_hit_energy);
            cluster->setLGAmp(noise_hit_energy * m_HG_ADC_to_LG_ADC);
            cluster->setErrorX(m_resolutionX);
            cluster->setErrorY(m_resolutionY);
            deviceData[noiseHitCandidate.second->layer - 1]->push_back(cluster);
        }
    }


    for (unsigned int nlayer = 0; nlayer < m_numberOfLayers; nlayer++) {
        clipboard->put(detectorIDs[nlayer], "clusters", (Objects*)deviceData[nlayer]);
    }

    // Advance to next event if possible, otherwise end this run:
    readEvents++;
    if (readEvents < m_inputTree->GetEntries()) return Success;
    else {
        LOG(INFO) << "No more events in data stream.";
        return Failure;
    };

}

void HGCalSimNtupleReader::finalise() {
    LOG(DEBUG) << "Finalise";
    m_inputFile->Close();
}
