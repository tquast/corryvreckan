#ifndef GBLTracking_H
#define GBLTracking_H 1

#include <iostream>

#include <GblPoint.h>
#include <GblTrajectory.h>

#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TMatrixD.h"

#include "core/algorithm/Algorithm.h"
#include "objects/Cluster.h"
#include "objects/Pixel.h"
#include "objects/Track.h"

// X0 Si = 21.82/2.33 = 9.365 cm
#define X0_Si 93.65
// X0 Diamond
#define X0_Diamond 122.0
// X0 Al = 24.01/2.70 = 8.89 cm
#define X0_Al 88.9
// X0 Au = 0.3344 cm
#define X0_Au 3.344
// X0 Cu = 12.86/8.96 = 1.435 cm
#define X0_Cu 14.35
// X0 air = 36.66/1.204E-3 = 303.9 m
#define X0_Air 304200.0
// X0 Kapton =  40.56 / 1.42 = 28.56 cm
#define X0_Kapton 285.6
// X0 PCB (FR4)
// http://personalpages.to.infn.it/~tosello/EngMeet/ITSmat/SDD/SDD_G10FR4.html
#define X0_PCB 167.608

namespace corryvreckan {

    class GBLTracking : public Algorithm {

    public:
        // Constructors and destructors
        GBLTracking(Configuration config, std::vector<Detector*> detectors);
        ~GBLTracking() {}

        // Functions
        void initialise();
        StatusCode run(Clipboard* clipboard);
        void finalise();

        // Histograms for several devices
        std::map<std::string, TH2F*> plotPerDevice;

        // Single histograms
        TH1F* singlePlot;

        // Member variables
        int m_eventNumber;

    private:
        TMatrixD Jac5(double ds);
        double getTheta(double energy, double radlength, double total_radlength);
        TVectorD getScatterer(double energy, double radlength, double total_radlength);

        gbl::GblPoint getPoint(double dz, double radlength, const TVectorD& measurement, const TVectorD& resolution);
        gbl::GblPoint getPoint(double dz, double radlength);

        double m_totalMaterialBudget;
        double m_energy;
        double m_probability;

        // Contol histograms
        TH1F *trackChi2, *trackChi2ndof, *trackProb;

        std::map<std::string, TH1F*> shiftsX;
        std::map<std::string, TH1F*> shiftsY;
        std::map<std::string, TH1F*> angleX;
        std::map<std::string, TH1F*> angleY;
        std::map<std::string, TH1F*> residualsX;
        std::map<std::string, TH1F*> residualsY;
        std::map<std::string, TH1F*> pullsX;
        std::map<std::string, TH1F*> pullsY;
        std::map<std::string, TH1F*> kinks;
    };
}
#endif // GBLTracking_H
