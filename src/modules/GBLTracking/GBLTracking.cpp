#include "GBLTracking.h"
#include "TMatrixD.h"
#include "objects/GBLTrack.h"

using namespace corryvreckan;
using namespace std;

TMatrixD GBLTracking::Jac5(double ds) {
    /*
       straight line, no B-field
       track =
       q/p, x', y', x, y
       0,   1,  2,  3, 4
    */
    TMatrixD jac(5, 5);
    jac.UnitMatrix();
    jac[3][1] = ds; // x = xp * ds
    jac[4][2] = ds; // y = yp * ds
    return jac;
}

double GBLTracking::getTheta(double energy, double radlength, double total_radlength) {

    // Return the scattering distribution with Theta according to the Highland forumla
    // http://pdg.lbl.gov/2015/reviews/rpp2014-rev-passage-particles-matter.pdf (Equation 32.15)

    // Radiation length fraction with no unit (thickness / rad. length), particle energy in [GeV]
    return (0.0136 * sqrt(radlength) / energy * (1 + 0.038 * log(total_radlength)));
}

TVectorD GBLTracking::getScatterer(double energy, double radlength, double total_radlength) {
    TVectorD scat(2);

    double theta = getTheta(energy, radlength, total_radlength);
    scat[0] = 1.0 / (theta * theta);
    scat[1] = 1.0 / (theta * theta);

    return scat;
}

GBLTracking::GBLTracking(Configuration config, std::vector<Detector*> detectors)
    : Algorithm(std::move(config), std::move(detectors)) {

    m_energy = m_config.get<double>("beam_energy", Units::convert(120, "GeV"));
    m_probability = m_config.get<double>("fit_probability", 0.0);
}

gbl::GblPoint GBLTracking::getPoint(double dz, double radlength, const TVectorD& measurement, const TVectorD& resolution) {

    // Get the scattering part:
    gbl::GblPoint point = getPoint(dz, radlength);

    // Precision = 1/resolution^2
    TVectorD measPrec(2);
    measPrec[0] = 1.0 / resolution[0] / resolution[0];
    measPrec[1] = 1.0 / resolution[1] / resolution[1];

    // measurement plane == propagation plane
    // FIXME add correct rotation between measurement plane and propagation!
    TMatrixD proL2m(2, 2);
    proL2m.UnitMatrix();
    point.addMeasurement(proL2m, measurement, measPrec);

    return point;
}

gbl::GblPoint GBLTracking::getPoint(double dz, double radlength) {

    // Propagate:
    TMatrixD jacPointToPoint = Jac5(dz);
    gbl::GblPoint point(jacPointToPoint);

    // Add scatterer:
    TVectorD scat(2);
    scat.Zero(); // mean is zero
    TVectorD wscat = getScatterer(m_energy, radlength, m_totalMaterialBudget);
    point.addScatterer(scat, wscat);

    return point;
}

void GBLTracking::initialise() {

    // Calculate material budget for the whole setup:
    LOG(DEBUG) << "Calculating total material budget in the particle path...";

    // Add the planes as scatterer:
    for(const auto& det : get_detectors()) {
        LOG(TRACE) << "Adding x/X0 = " << det->getMaterialBudget() << " for detector " << det->name();
        m_totalMaterialBudget += det->getMaterialBudget();
    }

    // Add the air as scattering material:
    // WARNING this relies on z-ordering!
    double total_distance = (get_detectors().back()->displacementZ() - get_detectors().front()->displacementZ());
    LOG(TRACE) << "Adding x/X0 = " << (total_distance / X0_Air) << " (air) for total telescope length of " << total_distance;
    m_totalMaterialBudget += total_distance / X0_Air;

    LOG(DEBUG) << "Total track material budget x/X0 = " << m_totalMaterialBudget;

    // Set up histograms
    trackChi2 = new TH1F("trackChi2", "GBL fit chi2;GBL chi2;tracks", 150, 0, 150);
    trackChi2ndof = new TH1F("trackChi2ndof", "GBL fit chi2/ndf;GBL chi2/ndf;tracks", 100, 0, 50);
    trackProb = new TH1F("trackProb", "GBL fit probability;GBL fit probability;tracks", 100, 0, 1);

    for(auto& detector : get_detectors()) {
        std::string detectorID = detector->name();
        std::string name = "residualsX_" + detectorID;
        std::string title = "GBL residual at " + detectorID + ";x residual [#mum];tracks";
        residualsX[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -150, 150);
        name = "residualsY_" + detectorID;
        title = "GBL residual at " + detectorID + ";y residual [#mum];tracks";
        residualsY[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -150, 150);

        name = "pullsX_" + detectorID;
        title = "GBL pull at " + detectorID + ";x pull;tracks";
        pullsX[detectorID] = new TH1F(name.c_str(), name.c_str(), 100, -5, 5);
        name = "pullsY_" + detectorID;
        title = "GBL pull at " + detectorID + ";y pull;tracks";
        pullsY[detectorID] = new TH1F(name.c_str(), name.c_str(), 100, -5, 5);

        name = "angleX_" + detectorID;
        title = "GBL angle at " + detectorID + ";x angle [mrad];tracks";
        angleX[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -5, 5);
        name = "angleY_" + detectorID;
        title = "GBL angle at " + detectorID + ";y angle [mrad];tracks";
        angleY[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -5, 5);

        name = "shiftsX_" + detectorID;
        title = "GBL shift at " + detectorID + ";x shift [#mum];tracks";
        shiftsX[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -150, 150);
        name = "shiftsY_" + detectorID;
        title = "GBL shift at " + detectorID + ";y shift [#mum];tracks";
        shiftsY[detectorID] = new TH1F(name.c_str(), title.c_str(), 300, -150, 150);

        name = "kinks_" + detectorID;
        title = "GBL kink angle at " + detectorID + ";" + detectorID + " kink [mrad];tracks";
        kinks[detectorID] = new TH1F(name.c_str(), name.c_str(), 100, -0.02, 0.02);
    }
}

StatusCode GBLTracking::run(Clipboard* clipboard) {

    // Make new storage element:
    GBLTracks* gbltracks = new GBLTracks();

    // Loop over all tracks
    Tracks* tracks = (Tracks*)clipboard->get("tracks");
    if(tracks == NULL || tracks->empty()) {
        return NoData;
    }

    LOG(DEBUG) << "Event has " << tracks->size() << " tracks";
    for(auto& track : (*tracks)) {

        // GBL point and label vectors for the trajectory
        std::vector<gbl::GblPoint> trajectory_points;
        std::map<int, std::string> trajectory_labels;

        // We will be seeding the GBL trajectory with the basic track

        // Retrieve clusters:
        Clusters clusters = track->clusters();
        if(clusters.empty()) {
            continue;
        }
        if(clusters.size() > get_detectors().size()) {
            LOG(WARNING) << "More clusters on track than detectors in setup. Skipping.";
            continue;
        }

        // Calculate difference between cluster and seed track
        auto cluster_residual = [](Track* track, Cluster* cluster) -> TVectorD {
            ROOT::Math::XYZPoint intercept = track->intercept(cluster->globalZ());
            TVectorD measurement(2);
            measurement[0] = intercept.X() - cluster->globalX();
            measurement[1] = intercept.Y() - cluster->globalY();
            return measurement;
        };

        // Arc length along the trajectory:
        double arclength = clusters.front()->globalZ();

        auto scatterer = [&](Detector* detector, Track* track) -> gbl::GblPoint {
            // Calculate step length
            double step = detector->getIntercept(track).Z() - arclength;
            arclength = detector->getIntercept(track).Z();
            LOG(TRACE) << "  Scatterer position: " << detector->getIntercept(track).Z() << " on detector "
                       << detector->name() << ", step " << step;

            // Material budget of the detector this cluster is attached to:
            double radlength = detector->getMaterialBudget();
            LOG(TRACE) << "  Material budget: " << radlength << " x/X0";

            LOG(DEBUG) << "Adding plane at " << arclength << " (scatterer)";
            // Get a GBL point with these properties:
            return getPoint(step, radlength);
        };

        auto measurement = [&](Detector* detector, Track* track, Cluster* cluster) -> gbl::GblPoint {

            // Calculate step length
            double step = cluster->globalZ() - arclength;
            arclength = cluster->globalZ();
            LOG(TRACE) << "  Cluster position: " << cluster->globalZ() << " on detector " << cluster->detectorID()
                       << ", step " << step;

            auto measurement_residual = cluster_residual(track, cluster);
            LOG(TRACE) << "  Residual: " << measurement_residual[0] << " " << measurement_residual[1];

            // Measurement resolution
            TVectorD resolution(2);
            resolution[0] = cluster->errorX();
            resolution[1] = cluster->errorY();
            LOG(TRACE) << "  Resolution: " << resolution[0] << " " << resolution[1];

            // Material budget of the detector this cluster is attached to:
            double radlength = detector->getMaterialBudget();
            auto thickness_global = ((*detector->m_rotations) * PositionVector3D<Cartesian3D<double>>(0., 0., 1.)).Z();
            LOG(TRACE) << "  Material budget: " << radlength / thickness_global << " x/X0 (global frame)";

            LOG(DEBUG) << "Adding plane at " << arclength << " (scatterer + measurement)";
            // Get a GBL point with these properties:
            return getPoint(step, radlength, measurement_residual, resolution);
        };

        int measurement_plane = 0;
        // Loop over all other clusters of the track
        auto detectors = get_detectors();
        for(auto detector_it = detectors.begin(); detector_it != detectors.end(); detector_it++) {
            auto detectorID = (*detector_it)->name();
            auto cluster_it = std::find_if(clusters.begin(), clusters.end(), [detectorID](Cluster* m) -> bool {
                return m->detectorID() == detectorID;
            });

            double step;
            if(cluster_it == clusters.end()) {
                step = (*detector_it)->getIntercept(track).Z() - arclength;
            } else {
                step = (*cluster_it)->globalZ() - arclength;
            }

            // Always add air scatterers, except before the first plane:
            if(std::distance(detectors.begin(), detector_it) > 0) {
                // First, add air scatterers:
                // Propagate [mm] from 0 to 0.21 = 0.5 - 1/sqrt(12)
                double distance = 0.21 * step;
                arclength += distance;

                // Add volume scatterer:
                gbl::GblPoint air1 = getPoint(distance, 0.5 * step / X0_Air);
                trajectory_points.push_back(air1);
                LOG(TRACE) << "Added volume scatterer at " << arclength;

                // Propagate [mm] 0.58 = from 0.21 to 0.79 = 0.5 + 1/sqrt(12)
                distance = 0.58 * step;
                arclength += distance;

                // Factor 0.5 for the volume as it is split into two scatterers:
                gbl::GblPoint air2 = getPoint(distance, 0.5 * step / X0_Air);
                trajectory_points.push_back(air2);
                LOG(TRACE) << "Added volume scatterer at " << arclength;
            }

            // Then add the actual measurement:
            if(cluster_it == clusters.end()) {
                trajectory_points.push_back(scatterer(*detector_it, track));
            } else {
                gbl::GblPoint point = measurement(*detector_it, track, *cluster_it);

                // Add global derivatives for alignment:
                TMatrixD alignment_derivatives(2, 2);
                alignment_derivatives(0, 0) = 1.0; // dx/dx
                alignment_derivatives(1, 0) = 0.0; // dy/dx
                alignment_derivatives(0, 1) = 0.0; // dx/dy
                alignment_derivatives(1, 1) = 1.0; // dy/dy

                std::vector<int> globalLabels;
                globalLabels.push_back(1 + 2 * measurement_plane);
                globalLabels.push_back(2 + 2 * measurement_plane);

                LOG(TRACE) << "  Global labels: " << globalLabels.at(0) << " " << globalLabels.at(1);
                point.addGlobals(globalLabels, alignment_derivatives);
                // Add point to trajectory
                trajectory_points.push_back(point);
                trajectory_labels[trajectory_points.size()] = (*detector_it)->name();
                measurement_plane++;
            }
        }

        // Fill trajectory, fit track
        double Chi2;
        int Ndf;
        double lostWeight;
        gbl::GblTrajectory trajectory(trajectory_points, false); // curvature = false
        IFLOG(TRACE) {
            LOG(TRACE) << "Trajectory points:";
            trajectory.printPoints();
        }

        // Fitting
        trajectory.fit(Chi2, Ndf, lostWeight);
        LOG(DEBUG) << "Track Chi2: " << track->chi2() << ", Chi2/Ndof: " << track->chi2ndof();
        LOG(DEBUG) << "GBL   Chi2: " << Chi2 << ", Chi2/Ndof: " << Chi2 / Ndf;
        trackChi2->Fill(Chi2);
        trackChi2ndof->Fill(Chi2 / Ndf);

        double fit_probability = TMath::Prob(Chi2, Ndf);
        trackProb->Fill(fit_probability);

        // Skip tracks with a fit probability smaller than defined value
        if(fit_probability < m_probability) {
            continue;
        }

        // Add GBL track to storage element:
        GBLTrack* gbltrack = new GBLTrack(trajectory);
        gbltrack->chi2(Chi2);
        gbltrack->ndof(Ndf);
        gbltracks->push_back(gbltrack);

        // Look at fit results:
        for(auto& label : trajectory_labels) {
            TVectorD corrections(5);
            TMatrixDSym covariance(5);

            unsigned int ndata = 2;
            TVectorD residuals(2);
            TVectorD measurement_errors(2);
            TVectorD residuals_errors(2);
            TVectorD downweights(2);

            // Query the fit results
            trajectory.getResults(label.first, corrections, covariance);
            trajectory.getMeasResults(label.first, ndata, residuals, measurement_errors, residuals_errors, downweights);

            // GBL coordinate assignment:
            // track = q/p, x', y', x, y
            //        0,   1,  2,  3, 4

            angleX[label.second]->Fill(Units::convert(corrections[1], "mrad"));
            angleY[label.second]->Fill(Units::convert(corrections[2], "mrad"));
            shiftsX[label.second]->Fill(Units::convert(corrections[3], "um"));
            shiftsY[label.second]->Fill(Units::convert(corrections[4], "um"));

            residualsX[label.second]->Fill(Units::convert(residuals[0], "um"));
            residualsY[label.second]->Fill(Units::convert(residuals[1], "um"));
            pullsX[label.second]->Fill(residuals[0] / residuals_errors[0]);
            pullsY[label.second]->Fill(residuals[1] / residuals_errors[1]);
        }
    }

    // Store all GBL tracks on the clipboard
    if(!gbltracks->empty()) {
        clipboard->put("gbltracks", (TestBeamObjects*)gbltracks);
    }

    // Return value telling analysis to keep running
    return Success;
}

void GBLTracking::finalise() {}
