## GBLTracking
**Maintainer**: Simon Spannagel (<simon.spannagel@cern.ch>)  
**Status**: Functional  
**Input**: Tracks  
**Output**: GBLTracks  

#### Description
Algorithm which performs a track re-fit using General Broken Lines [@GBL]. It takes all tracks from the clipboard and constructs GBL trajectories from them, adding all detectors as scatterers and all on-track clusters as measurements. Associated clusters are disregarded. The air between the telescope planes is accounted for by adding two equivalent thin scatterers at the appropriate positions.

The width of the scattering angle distribution is calculated using the Highland formula as given in the PDG [@highland]. Scattering material is correctly treated by first calculating the total sum of the scattering material budget and only then distributing it by weighting with the individual contributions of the scatterers.

This algorithm requires information about the material budget for every detector. It should be made sure, that the corresponding `material_budget` parameter is set to the respective `x/X0` value for each of the telescope planes. If not set, the default defined in the `Detector` class is used.

The algorithm also directly adds global parameters and alignment derivatives, which allow telescope alignment using Millepede-II (using the `MillepedeAlignment` algorithm).

#### Dependencies

This algorithm requires an installation of General Broken Lines. GBL depends on Eigen3 and ROOT.

```
$ svn checkout http://svnsrv.desy.de/public/GeneralBrokenLines/tags/V02-01-03/cpp GeneralBrokenLines
$ mkdir GeneralBrokenLines/build && cd GeneralBrokenLines/build
$ cmake ..
$ make install
$ cd ../
$ export CMAKE_PREFIX_PATH=`pwd`
```

The last command allows CMake to discover this installation of GBL when compiling Corryvreckan.

#### Parameters
* `beam_energy` : Energy of the particle beam. Used to calculate the width of the scattering angle distribution in the Highland formula. Defaults to 120 GeV. The unit of energy should be added, with no unit given, the value is interpreted as MeV.
* `fit_probability` : Probability of the fit, calculated via $`TMath::Prob(Chi2/Ndof)`$. This setting can be used to discard GBL tracks with a bad fit before histograming and storage. Defaults to 0.0, i.e. all tracks are accepted.

#### Usage
The default configuration is equal to the following:

```ini
[DefaultDigitizer]
beam_energy = 120GeV
fit_probability = 0.0
```

#### References
[@GBL]: http://www.sciencedirect.com/science/article/pii/S0168900212000642  
[@highland]: http://pdg.lbl.gov/2017/reviews/rpp2016-rev-passage-particles-matter.pdf  
