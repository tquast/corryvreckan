#ifndef HGCALMIPTRACKING_H
#define HGCALMIPTRACKING_H 1

#include <iostream>
#include "TCanvas.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TF1.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Cluster.h"
#include "objects/Pixel.h"
#include "objects/Track.h"

namespace corryvreckan {
/** @ingroup Modules
 */
class HGCalMIPTracking : public Module {

public:
    // Constructors and destructors
    HGCalMIPTracking(Configuration config, std::vector<Detector*> detectors);
    ~HGCalMIPTracking() {}

    // Functions
    void initialise();
    StatusCode run(Clipboard* clipboard);
    void finalise();

private:

    int eventCounter;

    std::vector<std::string> detectors;

    //configuration
    bool typeSelection;
    bool noiseSelection;
    bool useLG;
    float energyMaxThreshold;
    int ignoreFirstNLayers;

    float min_HGAmp_MIP;
    float max_HGAmp_MIP;

    float rangeSlopeScan;
    int nSlopePoints;
    float rangeOffsetScan;
    int nOffsetPoints;

    float reference_z;
    float maxDistance_reference;       //cm
    float maxDistance_final_reference; //cm
    float maxDistance_reference2;       //cm2
    float maxDistance_final_reference2;       //cm2

    int minLayers;
    bool keepTrackWithMostLayers;
    float NClustersPerLayerMax;
    float delta_chi2;
    float doubleCountRadius;
    float doubleCountRadius2;

    int cominatoricsCut;
    int traceFirstNEvents;


    bool prealignTranslationWithResiduals;
    std::map<std::string, TH1F*> residuals_x;
    std::map<std::string, TH1F*> residuals_y;



    //Book keeping
    TH2F* hist_x;
    TH2F* hist_y;
    TH2F* hist_offset;
    TH2F* hist_offset_occupancy;
    TH2F* hist_slope;
    TH2F* hist_slope_occupancy;

    TTree* m_outputEventTree;
    unsigned int eventID;
    bool track_found;
    float energySum;
    int NScanPoints;
    int NScanPointsXYMerged;
    int NTracksAfterLayerCut;
    int NTracksAfterClustersPerLayerCut;
    std::vector<int> NClusterCombinations;
    int NTracksAfterCombinatorics;
    int NTracksFinal;

    TTree* m_outputTrackTree;
    int track_NClusters;
    int track_NAssociatedClusters;
    float track_chi2;
    float track_bx;
    float track_by;
    float track_bz;
    float track_mx;
    float track_my;
    float track_mz;
    std::vector<unsigned int> cluster_layer_;
    std::vector<unsigned int> cluster_module_;
    std::vector<unsigned int> cluster_chip_;
    std::vector<unsigned int> cluster_channel_;
    std::vector<Float16_t> cluster_HGamp_;
    std::vector<Float16_t> cluster_LGamp_;
    std::vector<Float16_t> cluster_x_;
    std::vector<Float16_t> cluster_y_;
    std::vector<Float16_t> cluster_z_;
    std::vector<unsigned int> associated_cluster_layer_;
    std::vector<unsigned int> associated_cluster_module_;
    std::vector<unsigned int> associated_cluster_chip_;
    std::vector<unsigned int> associated_cluster_channel_;
    std::vector<Float16_t> associated_cluster_HGamp_;
    std::vector<Float16_t> associated_cluster_LGamp_;
    std::vector<Float16_t> associated_cluster_x_;
    std::vector<Float16_t> associated_cluster_y_;
    std::vector<Float16_t> associated_cluster_z_;

    std::chrono::steady_clock::time_point eventStartTime;
    unsigned int processingTime_mus;
};

} // namespace corryvreckan
#endif // HGCALMIPTRACKING_H
