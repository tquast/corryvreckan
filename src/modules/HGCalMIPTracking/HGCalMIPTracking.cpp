#include "HGCalMIPTracking.h"
#include "TCanvas.h"
#include "objects/KDTree.h"
#include <chrono>

using namespace corryvreckan;
using namespace std;



//comparison of tracks based on their chi2/ndof
bool compareTrackNLayers(Track* a, Track* b) { return (a->NLayers > b->NLayers); }
bool compareTrackNClustersPerLayer(Track* a, Track* b) { return (a->NClustersPerLayer < b->NClustersPerLayer); }
bool compareTrackChi2NDof(Track* a, Track* b) { return (a->chi2ndof() < b->chi2ndof()); }

//recursive function which constructs all possible connections of clusters to form a track
void product(int detectorIndex, vector<string> &detectors, map<string, Clusters*> &clusters, map<string, Cluster*> &cluster_memory, vector<Track*> &track_hypotheses, int minNClusters) {
    if (detectorIndex < detectors.size()) {
        if (clusters[detectors[detectorIndex]]->size() == 0) {
            cluster_memory[detectors[detectorIndex]] = NULL;       //the cluster_memorys map is always overwritten
            product(detectorIndex + 1, detectors, clusters, cluster_memory, track_hypotheses, minNClusters);
        } else {
            for (int clusterIndex = 0; clusterIndex < (int)clusters[detectors[detectorIndex]]->size(); clusterIndex++) {
                if (clusterIndex == -1) cluster_memory[detectors[detectorIndex]] = NULL;
                else cluster_memory[detectors[detectorIndex]] = clusters[detectors[detectorIndex]]->at(clusterIndex);       //the cluster_memorys map is always overwritten
                product(detectorIndex + 1, detectors, clusters, cluster_memory, track_hypotheses, minNClusters);
            }
        }
    } else {
        Track* track = new Track();
        for (int d = 0; d < detectors.size(); d++) if (cluster_memory[detectors[d]] != NULL) {
                track->addCluster(cluster_memory[detectors[d]]);
            }
        if (track->nClusters() >= minNClusters) {
            track->fit();    //at least minNClusters points for a fit, also calculates the chi2
            track_hypotheses.push_back(track);
        }
    }
}

HGCalMIPTracking::HGCalMIPTracking(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {

    useLG = m_config.get<bool>("useLowGain", false);
    energyMaxThreshold = m_config.get<float>("energyMaxThreshold", 10000.);
    typeSelection = m_config.get<bool>("typeSelection", false);
    noiseSelection = m_config.get<bool>("noiseSelection", false);
    ignoreFirstNLayers = m_config.get<int>("ignoreFirstNLayers", 0);

    min_HGAmp_MIP = m_config.get<float>("min_HGAmp_MIP", 15.);
    max_HGAmp_MIP = m_config.get<float>("max_HGAmp_MIP", 200.);

    reference_z = m_config.get<float>("reference_z", 0.0);

    rangeSlopeScan = m_config.get<float>("rangeSlopeScan", 0.05);
    nSlopePoints = m_config.get<int>("nSlopePoints", 100);

    rangeOffsetScan = m_config.get<float>("rangeOffsetScan", 0.5);  //cm

    nOffsetPoints = m_config.get<int>("nOffsetPoints", 10);
    maxDistance_reference = m_config.get<float>("maxDistance_reference", 5);  //cm
    maxDistance_final_reference = m_config.get<float>("maxDistance_final_reference", 5);  //cm

    minLayers = m_config.get<int>("minLayers", 10);
    keepTrackWithMostLayers = m_config.get<bool>("keepTrackWithMostLayers", true);
    NClustersPerLayerMax =  m_config.get<float>("NClustersPerLayerMax", -1);
    cominatoricsCut = m_config.get<int>("cominatoricsCut", 100000);
    delta_chi2 =  m_config.get<float>("delta_chi2", 0);
    doubleCountRadius = m_config.get<float>("doubleCountRadius", 1.5);

    doubleCountRadius2 = pow(doubleCountRadius, 2);
    maxDistance_reference2 = pow(maxDistance_reference, 2);
    maxDistance_final_reference2 = pow(maxDistance_final_reference, 2);

    traceFirstNEvents = m_config.get<int>("traceFirstNEvents", 10);

    prealignTranslationWithResiduals = m_config.get<bool>("prealignTranslationWithResiduals", false);
    eventCounter = 0;
}

void HGCalMIPTracking::initialise() {


    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();
        detectors.push_back(detectorID);
        residuals_x[detectorID] = new TH1F(("residualx_" + detectorID).c_str(), ("residualx_" + detectorID).c_str(), 100, -2. * Units::get("cm"), 2. * Units::get("cm"));
        residuals_y[detectorID] = new TH1F(("residualy_" + detectorID).c_str(), ("residualy_" + detectorID).c_str(), 100, -2. * Units::get("cm"), 2. * Units::get("cm"));
    }

    std::string m_treeName = "HGCalTracking_Algorithm";
    m_outputEventTree = new TTree(m_treeName.c_str(), m_treeName.c_str());
    LOG(INFO) << "Created tree: " << m_treeName;

    m_outputEventTree->Branch("eventID", &eventID);
    m_outputEventTree->Branch("processingTime_mus", &processingTime_mus);
    m_outputEventTree->Branch("track_found", &track_found);
    m_outputEventTree->Branch("energySum", &energySum);
    m_outputEventTree->Branch("NScanPoints", &NScanPoints);
    m_outputEventTree->Branch("NScanPointsXYMerged", &NScanPointsXYMerged);
    m_outputEventTree->Branch("NTracksAfterLayerCut", &NTracksAfterLayerCut);
    m_outputEventTree->Branch("NTracksAfterClustersPerLayerCut", &NTracksAfterClustersPerLayerCut);
    m_outputEventTree->Branch("NClusterCombinations", &NClusterCombinations);
    m_outputEventTree->Branch("NTracksAfterCombinatorics", &NTracksAfterCombinatorics);
    m_outputEventTree->Branch("NTracksFinal", &NTracksFinal);


    m_treeName = "HGCalTracking_Tracks";
    m_outputTrackTree = new TTree(m_treeName.c_str(), m_treeName.c_str());
    LOG(INFO) << "Created tree: " << m_treeName;
    m_outputTrackTree->Branch("eventID", &eventID);
    m_outputTrackTree->Branch("track_NClusters", &track_NClusters);
    m_outputTrackTree->Branch("track_NAssociatedClusters", &track_NAssociatedClusters);
    m_outputTrackTree->Branch("track_chi2", &track_chi2);
    m_outputTrackTree->Branch("track_bx", &track_bx);
    m_outputTrackTree->Branch("track_by", &track_by);
    m_outputTrackTree->Branch("track_bz", &track_bz);
    m_outputTrackTree->Branch("track_mx", &track_mx);
    m_outputTrackTree->Branch("track_my", &track_my);
    m_outputTrackTree->Branch("track_mz", &track_mz);
    m_outputTrackTree->Branch("cluster_layer", &cluster_layer_);
    m_outputTrackTree->Branch("cluster_module", &cluster_module_);
    m_outputTrackTree->Branch("cluster_chip", &cluster_chip_);
    m_outputTrackTree->Branch("cluster_channel", &cluster_channel_);
    m_outputTrackTree->Branch("cluster_HGamp", &cluster_HGamp_);
    m_outputTrackTree->Branch("cluster_LGamp", &cluster_LGamp_);
    m_outputTrackTree->Branch("cluster_x", &cluster_x_);
    m_outputTrackTree->Branch("cluster_y", &cluster_y_);
    m_outputTrackTree->Branch("cluster_z", &cluster_z_);
    m_outputTrackTree->Branch("associated_cluster_layer", &associated_cluster_layer_);
    m_outputTrackTree->Branch("associated_cluster_module", &associated_cluster_module_);
    m_outputTrackTree->Branch("associated_cluster_chip", &associated_cluster_chip_);
    m_outputTrackTree->Branch("associated_cluster_channel", &associated_cluster_channel_);
    m_outputTrackTree->Branch("associated_cluster_HGamp", &associated_cluster_HGamp_);
    m_outputTrackTree->Branch("associated_cluster_LGamp", &associated_cluster_LGamp_);
    m_outputTrackTree->Branch("associated_cluster_x", &associated_cluster_x_);
    m_outputTrackTree->Branch("associated_cluster_y", &associated_cluster_y_);
    m_outputTrackTree->Branch("associated_cluster_z", &associated_cluster_z_);

}

StatusCode HGCalMIPTracking::run(Clipboard* clipboard) {
    LOG(DEBUG) << "Start of event";
    eventCounter++;
    eventStartTime = std::chrono::steady_clock::now();

    eventID = eventCounter;
    energySum = 0;
    track_found = false;
    NScanPoints = 0;
    NScanPointsXYMerged = 0;
    NTracksAfterLayerCut = 0;
    NTracksAfterClustersPerLayerCut = 0;
    NClusterCombinations.clear();
    NTracksAfterCombinatorics = 0;
    NTracksFinal = 0;

    /*******   Step 0: Check that the core assumptions for this tracking scheme are fulfilled    *******/

    // If there are no detectors then stop trying to track
    if (detectors.size() == 0) {
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        return Failure;
    }

    // If there are less than three detectors then stop trying to track
    if (detectors.size() < 3) {
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        return Failure;
    }

    /*******   Step 1: loading the clusters from the detectors    *******/
    map<string, Clusters*> clusters;

    unsigned int N_clusters_for_tracking = 0;
    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();

        // Get the clusters
        Clusters* tempClusters = (Clusters*)clipboard->get(detectorID, "clusters");
        Clusters* selectedMIPClusters = new Clusters;

        for (int i = 0; i < tempClusters->size(); i++) {
            float HGEquivalent = useLG ? tempClusters->at(i)->getLGAmp() * 8 : tempClusters->at(i)->getHGAmp();
            energySum = energySum + HGEquivalent;
            if (HGEquivalent > max_HGAmp_MIP) continue;
            if (HGEquivalent < min_HGAmp_MIP) continue;
            if (noiseSelection && tempClusters->at(i)->getIsNoisy()) continue;
            if (typeSelection && tempClusters->at(i)->getPixeltype() != 0) continue;
            if (ignoreFirstNLayers >= tempClusters->at(i)->getLayer()) continue;

            selectedMIPClusters->push_back(tempClusters->at(i));
        }
        // Store them
        //LOG(DEBUG) << "Picked up " << selectedMIPClusters->size() << " / " << tempClusters->size() << " clusters from " << detectorID;
        clusters[detectorID] = selectedMIPClusters;
        N_clusters_for_tracking += selectedMIPClusters->size();
    }

    LOG(DEBUG) << "Total energy in event " << energySum << "  with number of selected clusters: " << N_clusters_for_tracking;
    if (energySum > energyMaxThreshold) {
        LOG(DEBUG) << "Event is skipped";
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        return Success;
    }

    /*******   Step 2: Construct all possible track hypotheses    *******/
    std::vector<Track*> track_hypotheses_x;
    std::vector<Track*> track_hypotheses_y;

    float dslope = rangeSlopeScan * 2 / nSlopePoints;
    float doffset = rangeOffsetScan * 2 / nOffsetPoints;
    float slope = -rangeSlopeScan;
    for (int slopeIndex = 0; slopeIndex <= nSlopePoints; slopeIndex++) {
        float offset = -rangeOffsetScan;
        for (int offsetIndex = 0; offsetIndex <= nOffsetPoints; offsetIndex++) {
            Track* track_x = new Track();
            track_x->m_state.SetX(offset);
            track_x->m_state.SetY(0);
            track_x->m_state.SetZ(reference_z);
            track_x->m_direction.SetX(slope);
            track_x->m_direction.SetY(0);
            track_x->m_direction.SetZ(1);
            track_hypotheses_x.push_back(track_x);

            Track* track_y = new Track();
            track_y->m_state.SetX(0);
            track_y->m_state.SetY(offset);
            track_y->m_state.SetZ(reference_z);
            track_y->m_direction.SetX(0);
            track_y->m_direction.SetY(slope);
            track_y->m_direction.SetZ(1);
            track_hypotheses_y.push_back(track_y);

            offset = offset + doffset;
        }
        slope = slope + dslope;
    }
    NScanPoints = track_hypotheses_x.size();
    LOG(DEBUG) << "Total number of scan points for each coordinate " << NScanPoints;



    /*******   Step 3: Evaluate all possible track hypotheses for x and y independently based on number of contributing layers  *******/
    if (eventCounter < traceFirstNEvents) {
        hist_x = new TH2F(("track_x" + std::to_string(eventCounter)).c_str(), ("track_x" + std::to_string(eventCounter)).c_str(), nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2, nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2);
        hist_y = new TH2F(("track_y" + std::to_string(eventCounter)).c_str(), ("track_y" + std::to_string(eventCounter)).c_str(), nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2, nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2);
    }

    //add the clusters to all tracks
    for (int ntrack = 0; ntrack < track_hypotheses_x.size(); ntrack++) {
        for (map<string, Clusters*>::iterator cluster_iterator = clusters.begin(); cluster_iterator != clusters.end(); cluster_iterator++) {        //loop over all layers
            bool found_hit_x = false;
            bool found_hit_y = false;
            for (int i = 0; i < cluster_iterator->second->size(); i++)  { //loop over all selected clusters
                if (!found_hit_x) {
                    double trackX = track_hypotheses_x[ntrack]->m_state.X() + track_hypotheses_x[ntrack]->m_direction.X() * cluster_iterator->second->at(i)->globalZ();
                    double dx = fabs(trackX - cluster_iterator->second->at(i)->globalX());
                    if (dx < maxDistance_reference) {
                        found_hit_x = true;
                        track_hypotheses_x[ntrack]->NLayers++;
                    }
                }
                if (!found_hit_y) {
                    double trackY = track_hypotheses_y[ntrack]->m_state.Y() + track_hypotheses_y[ntrack]->m_direction.Y() * cluster_iterator->second->at(i)->globalZ();
                    double dy = fabs(trackY - cluster_iterator->second->at(i)->globalY());
                    if (dy < maxDistance_reference) {
                        found_hit_y = true;
                        track_hypotheses_y[ntrack]->NLayers++;
                    }
                }
            }
        }
    }

    if (eventCounter < traceFirstNEvents) {
        for (int ntrack = 0; ntrack < track_hypotheses_x.size(); ntrack++) {
            hist_x->Fill(track_hypotheses_x[ntrack]->m_state.X(), track_hypotheses_x[ntrack]->m_direction.X(), track_hypotheses_x[ntrack]->NLayers);
            hist_y->Fill(track_hypotheses_y[ntrack]->m_state.Y(), track_hypotheses_y[ntrack]->m_direction.Y(), track_hypotheses_y[ntrack]->NLayers);
        }
    }


    /*******   Step 4: Combine x and y hypotheses selecting only those with a minimal number of layers in the 1-d tracks  *******/
    std::vector<Track*> track_hypotheses_full;
    for (int ntrack_x = 0; ntrack_x < track_hypotheses_x.size(); ntrack_x++) {
        if (track_hypotheses_x[ntrack_x]->NLayers < minLayers) continue;
        for (int ntrack_y = 0; ntrack_y < track_hypotheses_y.size(); ntrack_y++) {
            if (track_hypotheses_y[ntrack_y]->NLayers < minLayers) continue;

            Track* track_full = new Track();
            track_full->m_state.SetX(track_hypotheses_x[ntrack_x]->m_state.X());
            track_full->m_state.SetY(track_hypotheses_y[ntrack_y]->m_state.Y());
            track_full->m_state.SetZ(reference_z);
            track_full->m_direction.SetX(track_hypotheses_x[ntrack_x]->m_direction.X());
            track_full->m_direction.SetY(track_hypotheses_y[ntrack_y]->m_direction.Y());
            track_full->m_direction.SetZ(1);

            track_hypotheses_full.push_back(track_full);
        }
    }

    NScanPointsXYMerged = track_hypotheses_full.size();
    LOG(DEBUG) << "Number of combined tracks from x and y scan: " << NScanPointsXYMerged;


    /*******   Step 5: Discard events if no full tracks were made based on number of contributing layers  *******/
    if (track_hypotheses_full.size() == 0) {
        LOG(DEBUG) << "No MIP tracks detected!" << std::endl;
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        for (std::vector< Track* >::iterator it = track_hypotheses_full.begin() ; it != track_hypotheses_full.end(); ++it) delete (*it); track_hypotheses_full.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_x.begin() ; it != track_hypotheses_x.end(); ++it) delete (*it); track_hypotheses_x.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_y.begin() ; it != track_hypotheses_y.end(); ++it) delete (*it); track_hypotheses_y.clear();
        for (auto& detector : get_detectors()) {
            delete clusters[detector->name()];
        }
        return Success;
    }

    /*******   Step 6: Evaluate all possible full-track hypotheses  *******/
    if (eventCounter < traceFirstNEvents) {
        hist_offset = new TH2F(("track_offset" + std::to_string(eventCounter)).c_str(), ("track_offset" + std::to_string(eventCounter)).c_str(), nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2, nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2);
        hist_offset_occupancy = new TH2F(("track_offset_occupancy" + std::to_string(eventCounter)).c_str(), ("track_offset_occupancy" + std::to_string(eventCounter)).c_str(), nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2, nOffsetPoints + 1, -rangeOffsetScan - doffset / 2, rangeOffsetScan + doffset / 2);
        hist_slope = new TH2F(("track_slope" + std::to_string(eventCounter)).c_str(), ("track_slope" + std::to_string(eventCounter)).c_str(), nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2, nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2);
        hist_slope_occupancy = new TH2F(("track_slope_occupancy" + std::to_string(eventCounter)).c_str(), ("track_slope_occupancy" + std::to_string(eventCounter)).c_str(), nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2, nSlopePoints + 1, -rangeSlopeScan - dslope / 2, rangeSlopeScan + dslope / 2);
    }

    for (int ntrack = 0; ntrack < track_hypotheses_full.size(); ntrack++) {
        for (map<string, Clusters*>::iterator cluster_iterator = clusters.begin(); cluster_iterator != clusters.end(); cluster_iterator++) {        //loop over all layers
            bool layer_handled = false;
            for (int i = 0; i < cluster_iterator->second->size(); i++)  { //loop over all selected clusters
                if (track_hypotheses_full[ntrack]->distance2(cluster_iterator->second->at(i)) < maxDistance_reference2) {
                    track_hypotheses_full[ntrack]->addCluster(cluster_iterator->second->at(i));
                    if (layer_handled) continue;
                    track_hypotheses_full[ntrack]->NLayers++;
                    layer_handled = true;
                }
            }
        }
        if (eventCounter < traceFirstNEvents) {
            hist_offset->Fill(track_hypotheses_full[ntrack]->m_state.X(), track_hypotheses_full[ntrack]->m_state.Y(), track_hypotheses_full[ntrack]->NLayers);
            hist_offset_occupancy->Fill(track_hypotheses_full[ntrack]->m_state.X(), track_hypotheses_full[ntrack]->m_state.Y(), 1);
            hist_slope->Fill(track_hypotheses_full[ntrack]->m_direction.X(), track_hypotheses_full[ntrack]->m_direction.Y(), track_hypotheses_full[ntrack]->NLayers);
            hist_slope_occupancy->Fill(track_hypotheses_full[ntrack]->m_direction.X(), track_hypotheses_full[ntrack]->m_direction.Y(), 1);
        }
    }


    if (eventCounter < traceFirstNEvents) {
        hist_offset->Divide(hist_offset_occupancy);
        hist_slope->Divide(hist_slope_occupancy);
    }


    /*******   Step 7: Pick tracks with number of contributing layers above the minumum, discard the rest    *******/
    sort( track_hypotheses_full.begin( ), track_hypotheses_full.end( ), compareTrackNLayers);
    int NLayersMax = track_hypotheses_full[0]->NLayers;
    if (NLayersMax < minLayers) {
        LOG(DEBUG) << "No MIP tracks detected!" << std::endl;
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        for (std::vector< Track* >::iterator it = track_hypotheses_full.begin() ; it != track_hypotheses_full.end(); ++it) delete (*it); track_hypotheses_full.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_x.begin() ; it != track_hypotheses_x.end(); ++it) delete (*it); track_hypotheses_x.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_y.begin() ; it != track_hypotheses_y.end(); ++it) delete (*it); track_hypotheses_y.clear();
        for (auto& detector : get_detectors()) {
            delete clusters[detector->name()];
        }
        return Success;
    }

    int minLayersEvent = keepTrackWithMostLayers ? NLayersMax : minLayers;
    NTracksAfterLayerCut = 0;
    for (int i = NScanPointsXYMerged - 1; i > 0; --i) {
        if (track_hypotheses_full[i]->NLayers < minLayersEvent) {NTracksAfterLayerCut = i; delete track_hypotheses_full[i];}
    }
    LOG(DEBUG) << "number of hypothesis to keep after layer cut: " << NTracksAfterLayerCut << "(" << NScanPointsXYMerged << " before)";
    track_hypotheses_full.resize(NTracksAfterLayerCut);



    /*******   Step 8:  compute clusters per layer and keep tracks below a certain value   *******/
    NTracksAfterClustersPerLayerCut = NTracksAfterLayerCut;

    if (NClustersPerLayerMax != -1) {
        for (int ntrack = 0; ntrack < NTracksAfterLayerCut; ntrack++) track_hypotheses_full[ntrack]->computeClusterPerLayer();
        sort( track_hypotheses_full.begin( ), track_hypotheses_full.end( ), compareTrackNClustersPerLayer);

        for (int i = NTracksAfterLayerCut - 1; i > 0; --i) {
            if ((float)track_hypotheses_full[i]->NClustersPerLayer > NClustersPerLayerMax) {NTracksAfterClustersPerLayerCut = i; delete track_hypotheses_full[i];}
        }
        LOG(DEBUG) << "number of hypothesis to keep atfer clusters per layer cut: " << NTracksAfterClustersPerLayerCut << "(" << NTracksAfterLayerCut << " before)";
        track_hypotheses_full.resize(NTracksAfterClustersPerLayerCut);
    }

    if (NTracksAfterClustersPerLayerCut == 0) {
        LOG(DEBUG) << "No MIP tracks detected!" << std::endl;
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        for (std::vector< Track* >::iterator it = track_hypotheses_full.begin() ; it != track_hypotheses_full.end(); ++it) delete (*it); track_hypotheses_full.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_x.begin() ; it != track_hypotheses_x.end(); ++it) delete (*it); track_hypotheses_x.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_y.begin() ; it != track_hypotheses_y.end(); ++it) delete (*it); track_hypotheses_y.clear();
        for (auto& detector : get_detectors()) {
            delete clusters[detector->name()];
        }
        return Success;
    }

    /*******   Step 9: For each full track, combine all possible distance selected clusters to make all final track hypotheses    *******/
    map<string, Cluster*> cluster_memory;
    std::vector<Track*> track_hypotheses_final;

    for (int ntrack = 0; ntrack < NTracksAfterClustersPerLayerCut; ntrack++) {
        map<string, Clusters*> selectedClustersPerLayer;
        for (auto& detector : get_detectors()) selectedClustersPerLayer[detector->name()] = new Clusters;

        Clusters TrackClusters = track_hypotheses_full[ntrack]->clusters();
        for (auto& TrackCluster : TrackClusters) {
            selectedClustersPerLayer[TrackCluster->getDetectorID()]->push_back(TrackCluster);
        }

        int Ncombinations = 1;
        for (map<string, Clusters*>::iterator cluster_iterator = clusters.begin(); cluster_iterator != clusters.end(); cluster_iterator++) {
            Ncombinations = selectedClustersPerLayer[cluster_iterator->first]->size() > 0 ? Ncombinations * (selectedClustersPerLayer[cluster_iterator->first]->size()) : Ncombinations;
            if (Ncombinations > cominatoricsCut) break;
        }

        NClusterCombinations.push_back(Ncombinations);
        //LOG(DEBUG) << "Track " << ntrack << "/" << track_hypotheses_full.size() << "  Number of combinations: " << Ncombinations;
        if (Ncombinations <= cominatoricsCut)product(0, detectors, selectedClustersPerLayer, cluster_memory, track_hypotheses_final, minLayersEvent);
        else {
            LOG(DEBUG) << "Event: " << eventCounter << " with high number of combinatorics before the product, at least: " << Ncombinations;
        }

        for (map<string, Clusters*>::iterator it = selectedClustersPerLayer.begin() ; it != selectedClustersPerLayer.end(); ++it) delete (it->second); selectedClustersPerLayer.clear();
    }


    NTracksAfterCombinatorics = track_hypotheses_final.size();
    LOG(DEBUG) << "Number of tracks before overlap removal: " << NTracksAfterCombinatorics;
    if (NTracksAfterCombinatorics == 0) {
        LOG(DEBUG) << "No MIP tracks detected!" << std::endl;
        processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
        m_outputEventTree->Fill();
        for (std::vector< Track* >::iterator it = track_hypotheses_final.begin() ; it != track_hypotheses_final.end(); ++it) delete (*it); track_hypotheses_final.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_full.begin() ; it != track_hypotheses_full.end(); ++it) delete (*it); track_hypotheses_full.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_x.begin() ; it != track_hypotheses_x.end(); ++it) delete (*it); track_hypotheses_x.clear();
        for (std::vector< Track* >::iterator it = track_hypotheses_y.begin() ; it != track_hypotheses_y.end(); ++it) delete (*it); track_hypotheses_y.clear();
        for (auto& detector : get_detectors()) {
            delete clusters[detector->name()];
        }
        return Success;
    }

    /*******   Step 10: Sort final tracks by chi2 from straight line fit, keep only those within a certain distance of chi2 and avoid double counting by interception constraint   *******/

    sort( track_hypotheses_final.begin( ), track_hypotheses_final.end( ), compareTrackChi2NDof);

    Tracks* best_tracks = new Tracks;
    double best_chi2 = track_hypotheses_final[0]->chi2ndof();
    for (int i = 0; i < NTracksAfterCombinatorics; i++) {
        if (track_hypotheses_final[i]->chi2ndof() > best_chi2 + delta_chi2) break;

        bool distance_rejection = false;
        ROOT::Math::XYZPoint ICP1 = track_hypotheses_final[i]->intercept(reference_z);

        for (int j = 0; j < best_tracks->size(); j++) {
            ROOT::Math::XYZPoint ICP2 = best_tracks->at(j)->intercept(reference_z);

            if (pow(ICP1.x() - ICP2.x(), 2) + pow(ICP1.y() - ICP2.y(), 2) < doubleCountRadius2) {
                distance_rejection = true;
                break;
            }
        }

        if (!distance_rejection) best_tracks->push_back(new Track(track_hypotheses_final[i]));
    }
    NTracksFinal = (int)best_tracks->size();
    LOG(DEBUG) << "Number of final tracks after overlap removal: " << NTracksFinal ;

    /*******   Step 11: Use best track only for alignment    *******/
    Track* best_track = new Track(track_hypotheses_final[0]);
    track_found = true;
    Clusters bestTrackClusters = best_track->clusters();
    for (auto& trackCluster : bestTrackClusters) {
        string detectorID = trackCluster->detectorID();
        ROOT::Math::XYZPoint intercept = best_track->intercept(trackCluster->globalZ());
        residuals_x[detectorID]->Fill(intercept.X() - trackCluster->globalX());
        residuals_y[detectorID]->Fill(intercept.Y() - trackCluster->globalY());
    }


    /*******   Step 12: Add associated clusters based on the bounds of the track and the distance  *******/
    //start from the track with the highest number of layers
    sort( best_tracks->begin( ), best_tracks->end( ), compareTrackNLayers);

    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();

        // Get the clusters
        Clusters* tempClusters = (Clusters*)clipboard->get(detectorID, "clusters");
        for (int i = 0; i < tempClusters->size(); i++) {
            for (int j = 0; j < NTracksFinal; j++) {
                if (tempClusters->at(i)->getLayer() > best_tracks->at(j)->endLayer()) continue; //make sure that the selected cluster is within the bounds of the track
                if (best_tracks->at(j)->distance2(tempClusters->at(i)) < maxDistance_final_reference2) {
                    best_tracks->at(j)->addAssociatedCluster(tempClusters->at(i));
                }
            }
        }
    }


    /*******   Step 13: Write out tracks & clusters   *******/
    for (int i = 0; i < NTracksFinal; i++) {
        track_chi2 = best_tracks->at(i)->chi2ndof();
        track_bx = best_tracks->at(i)->m_state.X() / Units::get("cm");
        track_by = best_tracks->at(i)->m_state.Y() / Units::get("cm");
        track_bz = best_tracks->at(i)->m_state.Z() / Units::get("cm");
        track_mx = best_tracks->at(i)->m_direction.X();
        track_my = best_tracks->at(i)->m_direction.Y();
        track_mz = best_tracks->at(i)->m_direction.Z();
        track_NClusters = (int)best_tracks->at(i)->clusters().size();
        track_NAssociatedClusters = (int)best_tracks->at(i)->associatedClusters().size();
        cluster_layer_.clear();
        cluster_module_.clear();
        cluster_chip_.clear();
        cluster_channel_.clear();
        cluster_HGamp_.clear();
        cluster_LGamp_.clear();
        cluster_x_.clear();
        cluster_y_.clear();
        cluster_z_.clear();
        associated_cluster_layer_.clear();
        associated_cluster_module_.clear();
        associated_cluster_chip_.clear();
        associated_cluster_channel_.clear();
        associated_cluster_HGamp_.clear();
        associated_cluster_LGamp_.clear();
        associated_cluster_x_.clear();
        associated_cluster_y_.clear();
        associated_cluster_z_.clear();

        Clusters trackClusters = best_tracks->at(i)->clusters();
        for (auto& trackCluster : trackClusters) {
            cluster_layer_.push_back(trackCluster->getLayer());
            cluster_module_.push_back(trackCluster->getModule());
            cluster_chip_.push_back(trackCluster->getChip());
            cluster_channel_.push_back(trackCluster->getChannel());
            cluster_HGamp_.push_back(trackCluster->getHGAmp());
            cluster_LGamp_.push_back(trackCluster->getLGAmp());
            cluster_x_.push_back(trackCluster->localX() / Units::get("cm"));
            cluster_y_.push_back(trackCluster->localY() / Units::get("cm"));
            cluster_z_.push_back(trackCluster->globalZ() / Units::get("cm"));
        }

        Clusters associatedTrackClusters = best_tracks->at(i)->associatedClusters();
        for (auto& associatedTrackCluster : associatedTrackClusters) {
            associated_cluster_layer_.push_back(associatedTrackCluster->getLayer());
            associated_cluster_module_.push_back(associatedTrackCluster->getModule());
            associated_cluster_chip_.push_back(associatedTrackCluster->getChip());
            associated_cluster_channel_.push_back(associatedTrackCluster->getChannel());
            associated_cluster_HGamp_.push_back(associatedTrackCluster->getHGAmp());
            associated_cluster_LGamp_.push_back(associatedTrackCluster->getLGAmp());
            associated_cluster_x_.push_back(associatedTrackCluster->localX() / Units::get("cm"));
            associated_cluster_y_.push_back(associatedTrackCluster->localY() / Units::get("cm"));
            associated_cluster_z_.push_back(associatedTrackCluster->globalZ() / Units::get("cm"));
        }

        m_outputTrackTree->Fill();
    }


    clipboard->put("tracks", (Objects*)best_tracks);


    /*******   Last: cleanup   *******/
    delete best_track;
    //for (map<string, Clusters*>::iterator it = selectedClustersPerLayer.begin() ; it != selectedClustersPerLayer.end(); ++it) delete (it->second); selectedClustersPerLayer.clear();
    for (std::vector< Track* >::iterator it = track_hypotheses_final.begin() ; it != track_hypotheses_final.end(); ++it) delete (*it); track_hypotheses_final.clear();
    for (std::vector< Track* >::iterator it = track_hypotheses_full.begin() ; it != track_hypotheses_full.end(); ++it) delete (*it); track_hypotheses_full.clear();
    for (std::vector< Track* >::iterator it = track_hypotheses_x.begin() ; it != track_hypotheses_x.end(); ++it) delete (*it); track_hypotheses_x.clear();
    for (std::vector< Track* >::iterator it = track_hypotheses_y.begin() ; it != track_hypotheses_y.end(); ++it) delete (*it); track_hypotheses_y.clear();
    for (auto& detector : get_detectors()) {
        delete clusters[detector->name()];
    }
    LOG(DEBUG) << "End of event";
    processingTime_mus = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - eventStartTime).count();
    m_outputEventTree->Fill();
    return Success;
}

void HGCalMIPTracking::finalise() {
    if (prealignTranslationWithResiduals) {
        LOG(INFO) << "Performing prealignment based on full track residuals";
        //determine mean residuals
        TF1* gaus = new TF1("gaus", "gaus");

        for (int d = 0; d < detectors.size(); d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);

            double mean_x = residuals_x[detector_name]->GetXaxis()->GetBinCenter(residuals_x[detector_name]->GetMaximumBin());
            double rms_x = residuals_x[detector_name]->GetRMS();
            double mean_y = residuals_y[detector_name]->GetXaxis()->GetBinCenter(residuals_y[detector_name]->GetMaximumBin());
            double rms_y = residuals_y[detector_name]->GetRMS();
            gaus->SetParameter(0, residuals_x[detector_name]->GetMaximum());
            gaus->SetParameter(1, mean_x);
            gaus->SetParameter(2, rms_x);
            gaus->SetRange(mean_x - 2.0 * rms_x, mean_x + 2.0 * rms_x);
            residuals_x[detector_name]->Fit(gaus, "RQ");
            double displacementX = gaus->GetParameter(1);
            gaus->SetParameter(0, residuals_y[detector_name]->GetMaximum());
            gaus->SetParameter(1, mean_y);
            gaus->SetParameter(2, rms_y);
            gaus->SetRange(mean_y - 2.0 * rms_y, mean_y + 2.0 * rms_y);
            residuals_y[detector_name]->Fit(gaus, "RQ");
            double displacementY = gaus->GetParameter(1);
            LOG(INFO) << "Detector: " << detector_name;
            LOG(INFO) << "displacementX: " << displacementX;
            LOG(INFO) << "displacementY: " << displacementY;

            double x0 = detector->displacement().X();
            double y0 = detector->displacement().Y();
            detector->displacementX(x0 + displacementX);
            detector->displacementY(y0 + displacementY);
        }
        delete gaus;
    }
}
