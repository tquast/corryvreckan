## MIP Tracking
**Maintainer**: Thorben Quast (<thorben.quast@cern.ch>)   
**Status**: Experimental 

#### Description
A MIP tracking algorithm for the 2018 HGCAL prototype detector based on searches for straight line traversing as many layers as possible


#### Requirements
* Reconstructed data from the 2018 HGCAL prototype in ntuple format.
* Converted hits into corryvreckan clusters using the HGCalTBNtupleReader.


#### Usage for the 200 GeV/c muon beam data taken in October 2018
```toml
[HGCalMIPTracking]
typeSelection=true
noiseSelection=true
useLowGain=false
energyMaxThreshold=15000.
ignoreFirstNLayers=0
traceFirstNEvents=1
min_HGAmp_MIP=25
max_HGAmp_MIP=200
rangeSlopeScan= 0.2
nSlopePoints= 40
rangeOffsetScan= 20cm
nOffsetPoints= 40
reference_z = 65cm
maxDistance_reference = 1.2cm
maxDistance_final_reference = 1.2cm
delta_chi2 = 0
doubleCountRadius = 2.5cm
cominatoricsCut=1000000
NClustersPerLayerMax = 1.5
minLayers = 8
keepTrackWithMostLayers=true
```