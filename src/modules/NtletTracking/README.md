## NtletTracking
**Maintainer**: Thorben Quast (<thorben.quast@cern.ch>)   
**Status**: Functional 

#### Description
A tracking algorithm constructed for cases in which a non-negligible amount of absorber is placed in between the detector arms.
All possible tracks (Ntlets) are computed for each set of detectors on both sides of the absorbers. Those who agree well in their direction and their pointing at a reference plane are matched to a full track.
The kink angle distibution of pairs of Ntlets can also be used for tomography studies of the absorber in the middle
Last but not least, the modules provides suitable functionality for pre-alignment of configurations where two sets of detectors are significantly misaligned to each other.


An illustrative with all its features can be found here.
http://tquast.web.cern.ch/tquast/NtletTracking.pdf


#### Requirements
* Six tracking detectors or more. At least three on either side of the reference plane/the absorber.
* Tracks are computed from clusters


#### Parameters
* `N1`: Indicator for the number of detector planes to define the first set of Ntlet points. Default value is `3`.
* `N2`: Indicator for the number of detector planes to define the second set of Ntlet points. Indexing starts at N1+1. Default value is `6`.
* `chi2PerNDOFCut1`: Chi2/ndf cut for the first Ntlet hypotheses. Default value is `2000`.
* `chi2PerNDOFCut2`: Chi2/ndf cut for the second Ntlet hypotheses. Default value is `2000`.
* `NTracks1Max`: Maximum number of track hypothesis in the first set of Ntlet hypotheses. Default value is `5`.
* `NTracks2Max`: Maximum number of track hypothesis in the second set of Ntlet hypotheses. Default value is `5`.
* `zExtrapolate`: Position of the physical or hypothetical reference plane for the reference analysis and/or prealignment. Unit is mm. Default value is `360`.
* `centerX_ref/centerX_ref`: Expected mean pointing distance at the reference plane for a pair of Ntlets. Unit is mm. Default value is `0.0`.
* `maxDistance`: Maximum distance in the pointing at the reference plane for a pair of Ntlets to be combined. Unit is mm. Default value is `4.0`.
* `maxKinkAngle`: Maximum kink angle at the reference plane for a pair of Ntlets to be combined. Unit is rad. Default value is `0.01`.
* `prealignAnglesWithBeamLine`: Enable the prealignment (1) at the end of the analysis. Default value is `false`.
* `prealignTranslationWithBeamLine`: Enable the prealignment (2) at the end of the analysis. Default value is `false`.
* `prealignTranslationWithResiduals`: Enable the prealignment (3) at the end of the analysis. Default value is `false`.
* `prealignRotationWithDisplacement`: Enable the prealignment (4) at the end of the analysis. Default value is `false`.
* `performReferenceAnalysis`: Run a reference analysis producing the necessary tree and histograms for 2D tomography of the absorber at zExtrapolate. Default value is `false`.



#### Plots produced
The following quantities are filled into histograms based on full tracks ("track") and Ntlets tracks ("track1" and "track2", respectively).
* chi2 per ndf (1D)
* number of tracks per readout/event (1D)
* track angles w.r.t. the beam axis = z-axis (2D)
* residuals at every detector plane computed with the corresponding tracks as reference

* Discrepancy in the pointing of both Ntlets at the reference plane.

If the reference analysis is run:
* a TTree containing the impact positions and kink angles obtained from the Ntlets.
* kink angle (1D)
* occupancy (2d)
* mean kink angle as a function of the impact position (2D)

#### Usage
```toml
[NtletTracking]
N1=3
N2=6
chi2PerNDOFCut1=20.
chi2PerNDOFCut2=20.
NTracks1Max=5
NTracks2Max=5
zExtrapolate=370.
maxDistance=.1
maxKinkAngle=10.
prealignAnglesWithBeamLine=false
prealignTranslationWithBeamLine=false
prealignTranslationWithResiduals=false
prealignRotationWithDisplacement=false
performReferenceAnalysis=true

log_level=INFO

```