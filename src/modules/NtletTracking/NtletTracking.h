#ifndef NTLETTRACKING_H
#define NTLETTRACKING_H 1

#include <iostream>
#include "TCanvas.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TF1.h"
#include "TProfile.h"
#include "TTree.h"
#include "core/module/Module.hpp"
#include "objects/Cluster.h"
#include "objects/Pixel.h"
#include "objects/Track.h"

namespace corryvreckan {
    /** @ingroup Modules
     */
    class NtletTracking : public Module {

    public:
        // Constructors and destructors
        NtletTracking(Configuration config, std::vector<Detector*> detectors);
        ~NtletTracking() {}

        // Functions
        void initialise();
        StatusCode run(Clipboard* clipboard);
        void finalise();

    private:
        
        TH1F* track1Chi2ndof;
        TH1F* tracks1PerEvent;
        TH2F* track1Angle;

        TH1F* track2Chi2ndof;
        TH1F* tracks2PerEvent;
        TH2F* track2Angle;
        
        TH2F* displacementReference;
        TH2F* displacementReference_dXvsX;
        TH2F* displacementReference_dXvsY;
        TH2F* displacementReference_dYvsX;
        TH2F* displacementReference_dYvsY;

        TH1F* trackChi2ndof;
        TH1F* tracksPerEvent;
        TH2F* trackAngle;

        std::map<std::string, TH1F*> residualsNtletX;
        std::map<std::string, TH2F*> residualsNtletX_vs_X;
        std::map<std::string, TH2F*> residualsNtletX_vs_Y;
        
        std::map<std::string, TH1F*> residualsNtletY;
        std::map<std::string, TH2F*> residualsNtletY_vs_X;
        std::map<std::string, TH2F*> residualsNtletY_vs_Y;
        
        std::map<std::string, TH2F*> residualsNtlet;


        std::map<std::string, TH1F*> residualsX;
        std::map<std::string, TH2F*> residualsX_vs_X;
        std::map<std::string, TH2F*> residualsX_vs_Y;

        std::map<std::string, TH1F*> residualsY;
        std::map<std::string, TH2F*> residualsY_vs_X;
        std::map<std::string, TH2F*> residualsY_vs_Y;        

        std::map<std::string, TH2F*> residuals;
        
        TH2F* referenceHitMap;
        TH2F* referenceHitMapMeanAngularWeighted;

        TH1F* kinkAngle;

        
        TTree* kinkAngleTree;
        float _kinkAngle;
        float _kinkAngleX;
        float _kinkAngleY;
        float _impactDUT1X;
        float _impactDUT1Y;

        std::vector<std::string> detectors;

        //configurations
        int N1;
        int N2;
        double chi2PerNDOFCut1;
        double chi2PerNDOFCut2;
        int NTracks1Max;
        int NTracks2Max;
        double zExtrapolate;
        double maxDistance;
        double maxKinkAngle;
        double centerX_ref;
        double centerY_ref;

        bool prealignAnglesWithBeamLine;
        bool prealignTranslationWithBeamLine;
        bool prealignTranslationWithResiduals;
        bool prealignRotationWithDisplacement;
        bool performReferenceAnalysis;

    };
} // namespace corryvreckan
#endif // NTLETTRACKING_H
