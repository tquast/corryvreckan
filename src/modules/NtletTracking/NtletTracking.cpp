#include "NtletTracking.h"
#include "TCanvas.h"
#include "objects/KDTree.h"

using namespace corryvreckan;
using namespace std;


//recursive function which constructs all possible connections of clusters to form a track
void product(int detectorIndex, int minIndex, int maxIndex, vector<string> &detectors, map<string, Clusters*> &clusters, map<string, Cluster*> &cluster_memory, vector<Track*> &track_hypotheses) {
    if (detectorIndex < maxIndex) {
        for (int clusterIndex = 0; clusterIndex < clusters[detectors[detectorIndex]]->size(); clusterIndex++) {
            cluster_memory[detectors[detectorIndex]] = clusters[detectors[detectorIndex]]->at(clusterIndex);       //the cluster_memorys map is always overwritten
            product(detectorIndex + 1, minIndex, maxIndex, detectors, clusters, cluster_memory, track_hypotheses);
        }
    } else {
        Track* track = new Track();
        for (int d = minIndex; d < maxIndex; d++) track->addCluster(cluster_memory[detectors[d]]);
        if (track->nClusters() >= 3) track->fit();   //at least three points for a fit
        track_hypotheses.push_back(track);
    }
}

//comparison of tracks based on their chi2/ndof
bool compareTrackChi2NDof(Track* a, Track* b) { return (a->chi2ndof() < b->chi2ndof()); }


NtletTracking::NtletTracking(Configuration config, std::vector<Detector*> detectors)
    : Module(std::move(config), std::move(detectors)) {


    N1 = m_config.get<int>("N_Ntlet1", 3);
    N2 = m_config.get<int>("N_Ntlet2", 6);

    chi2PerNDOFCut1 = m_config.get<double>("chi2PerNDOFCut1", 2000.);
    chi2PerNDOFCut2 = m_config.get<double>("chi2PerNDOFCut2", 2000.);

    NTracks1Max = m_config.get<int>("NTracks1Max", 5);
    NTracks2Max = m_config.get<int>("NTracks2Max", 5);

    zExtrapolate = m_config.get<double>("zExtrapolate", 360.);   //mm
    maxDistance = m_config.get<double>("maxDistance", 4.0);   //mm
    maxKinkAngle = m_config.get<double>("maxKinkAngle", 0.01);   //rad
    centerX_ref = m_config.get<double>("centerX_ref", 0.0);     //m
    centerY_ref = m_config.get<double>("centerY_ref", 0.0);     //m


    prealignAnglesWithBeamLine = m_config.get<bool>("prealignAnglesWithBeamLine", false);
    prealignTranslationWithBeamLine = m_config.get<bool>("prealignTranslationWithBeamLine", false);
    prealignTranslationWithResiduals = m_config.get<bool>("prealignTranslationWithResiduals", false);
    prealignRotationWithDisplacement = m_config.get<bool>("prealignRotationWithDisplacement", false);
    performReferenceAnalysis = m_config.get<bool>("performReferenceAnalysis", false);

}

void NtletTracking::initialise() {


    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();
        detectors.push_back(detectorID);
    }


    // Set up histograms
    track1Chi2ndof = new TH1F("track1Chi2ndof", "track1Chi2ndof", 100, 0, chi2PerNDOFCut1);
    tracks1PerEvent = new TH1F("tracks1PerEvent", "tracks1PerEvent", NTracks1Max, 0, NTracks1Max);
    track1Angle = new TH2F("trackAngle1", "trackAngle1", 200, -0.01, 0.01, 200, -0.01, 0.01);

    track2Chi2ndof = new TH1F("track2Chi2ndof", "track2Chi2ndof", 100, 0, chi2PerNDOFCut2);
    tracks2PerEvent = new TH1F("tracks2PerEvent", "tracks2PerEvent", NTracks2Max, 0, NTracks2Max);
    track2Angle = new TH2F("trackAngle2", "trackAngle2", 200, -0.01, 0.01, 200, -0.01, 0.01);

    displacementReference = new TH2F("displacementAtReference", ("displacementAtReference at z=" + std::to_string(zExtrapolate)).c_str(), 200, centerX_ref - maxDistance, centerX_ref + maxDistance, 200, centerY_ref - maxDistance, centerY_ref + maxDistance);
    displacementReference_dXvsX = new TH2F("displacementReference_dXvsX", ("displacementReference_dXvsX at z=" + std::to_string(zExtrapolate)).c_str(), 200, -10, 10 , 200, centerX_ref - maxDistance, centerX_ref + maxDistance);
    displacementReference_dYvsY = new TH2F("displacementReference_dYvsY", ("displacementReference_dYvsY at z=" + std::to_string(zExtrapolate)).c_str(), 200, -5, 5 , 200, centerY_ref - maxDistance, centerY_ref + maxDistance);
    displacementReference_dXvsY = new TH2F("displacementReference_dXvsY", ("displacementReference_dXvsY at z=" + std::to_string(zExtrapolate)).c_str(), 200, -5, 5 , 200, centerX_ref - maxDistance, centerX_ref + maxDistance);
    displacementReference_dYvsX = new TH2F("displacementReference_dYvsX", ("displacementReference_dYvsX at z=" + std::to_string(zExtrapolate)).c_str(), 200, -10, 10 , 200, centerY_ref - maxDistance, centerY_ref + maxDistance);

    trackChi2ndof = new TH1F("trackChi2ndof", "trackChi2ndof", 100, 0, 50);
    tracksPerEvent = new TH1F("tracksPerEvent", "tracksPerEvent", NTracks1Max * NTracks2Max, 0, NTracks1Max * NTracks2Max);
    trackAngle = new TH2F("trackAngle", "trackAngle", 200, -0.01, 0.01, 200, -0.01, 0.01);


    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();

        string name = "residualsNtletX_" + detectorID;
        residualsNtletX[detectorID] = new TH1F(name.c_str(), name.c_str(), 5000, -1.0, 1.0);
        name = "residualsNtletX_vs_X_" + detectorID;
        residualsNtletX_vs_X[detectorID] = new TH2F(name.c_str(), name.c_str(), 2000, -10., 10., 500, -0.2, 0.2);
        name = "residualsNtletX_vs_Y_" + detectorID;
        residualsNtletX_vs_Y[detectorID] = new TH2F(name.c_str(), name.c_str(), 1000, -5., 5., 500, -0.2, 0.2);

        name = "residualsNtletY_" + detectorID;
        residualsNtletY[detectorID] = new TH1F(name.c_str(), name.c_str(), 5000, -1.0, 1.0);
        name = "residualsNtletY_vs_X_" + detectorID;
        residualsNtletY_vs_X[detectorID] = new TH2F(name.c_str(), name.c_str(), 2000, -10., 10., 500, -0.2, 0.2);
        name = "residualsNtletY_vs_Y_" + detectorID;
        residualsNtletY_vs_Y[detectorID] = new TH2F(name.c_str(), name.c_str(), 1000, -5., 5., 500, -0.2, 0.2);


        name = "residualsNtlet_" + detectorID;
        residualsNtlet[detectorID] = new TH2F(name.c_str(), name.c_str(), 5000, -1.0, 1.0, 5000, -1.0, 1.0);

        name = "residualsX_" + detectorID;
        residualsX[detectorID] = new TH1F(name.c_str(), name.c_str(), 5000, -1.0, 1.0);
        name = "residualsX_vs_X_" + detectorID;
        residualsX_vs_X[detectorID] = new TH2F(name.c_str(), name.c_str(), 2000, -10., 10., 500, -0.2, 0.2);
        name = "residualsX_vs_Y_" + detectorID;
        residualsX_vs_Y[detectorID] = new TH2F(name.c_str(), name.c_str(), 1000, -5., 5., 500, -0.2, 0.2);
        name = "residualsY_" + detectorID;
        residualsY[detectorID] = new TH1F(name.c_str(), name.c_str(), 5000, -1.0, 1.0);
        name = "residualsY_vs_X_" + detectorID;
        residualsY_vs_X[detectorID] = new TH2F(name.c_str(), name.c_str(), 2000, -10., 10., 500, -0.2, 0.2);
        name = "residualsY_vs_Y_" + detectorID;
        residualsY_vs_Y[detectorID] = new TH2F(name.c_str(), name.c_str(), 1000, -5., 5., 500, -0.2, 0.2);
        name = "residuals_" + detectorID;
        residuals[detectorID] = new TH2F(name.c_str(), name.c_str(), 5000, -1.0, 1.0, 5000, -1.0, 1.0);
    }


    if (performReferenceAnalysis) {
        kinkAngleTree = new TTree("kinkAngles", "kinkAngles");
        kinkAngleTree->Branch("kinkAngle", &_kinkAngle);
        kinkAngleTree->Branch("kinkAngleX", &_kinkAngleX);
        kinkAngleTree->Branch("kinkAngleY", &_kinkAngleY);
        kinkAngleTree->Branch("impactDUT1X", &_impactDUT1X);
        kinkAngleTree->Branch("impactDUT1Y", &_impactDUT1Y);

        kinkAngle = new TH1F("scatteringAngle", "scatteringAngle", 200, 0.0, 0.011);  //kink between triplet track 1 and triplet track 2
        referenceHitMap = new TH2F("referenceHitMap", "referenceHitMap", 80, -10., 10., 40, 5., 5.);
        referenceHitMapMeanAngularWeighted = new TH2F("referenceHitMapMeanAngularWeighted", "referenceHitMapMeanAngularWeighted", 80, -10., 10., 40, 5., 5.);
    }



}

StatusCode NtletTracking::run(Clipboard* clipboard) {
    LOG(DEBUG) << "Start of event";


    /*******   Step 0: Check that the core assumptions for this tracking scheme are fulfilled    *******/

    // If there are no detectors then stop trying to track
    if (detectors.size() == 0)
        return Success;

    // If there are less than six detectors then stop trying to track
    if (detectors.size() < 6)
        return Success;
    //require at least three detectors for the first Ntlet
    if (N1 < 3) {
        LOG(INFO) << "Need at least three planes in the first Ntlet. " << N1 << " are only configured.";
        return Success;
    }
    //require at least three detectors for the second Ntlet
    if (N2 - N1 < 3) {
        LOG(INFO) << "Need at least three planes in the second Ntlet. " << N2 - N1 << " are only configured.";
        return Success;
    }

    /*******   Step 1: loading the clusters from the detectors    *******/
    map<string, Clusters*> clusters;
    for (auto& detector : get_detectors()) {
        string detectorID = detector->name();

        // Get the clusters
        Clusters* tempClusters = (Clusters*)clipboard->get(detectorID, "clusters");
        if (tempClusters == NULL || tempClusters->size() == 0) {
            LOG(DEBUG) << "Detector " << detectorID << " does not have any clusters on the clipboard";
            return Success;
        } else {
            // Store them
            LOG(DEBUG) << "Picked up " << tempClusters->size() << " clusters from " << detectorID;

            clusters[detectorID] = tempClusters;
        }
    }



    /*******   Step 2: for each side, construct all possible permutations of points to form a straight line track ('Ntlet/track hypothesis')   *******/

    //compute the number of permutations
    int N_trackhypotheses1 = 1, N_trackhypotheses2 = 1;
    for (int d = 0; d < N1; d++) N_trackhypotheses1 *= clusters[detectors[d]]->size();
    for (int d = N1; d < N2; d++) N_trackhypotheses2 *= clusters[detectors[d]]->size();
    LOG(DEBUG) << "Number of possible hypotheses for the first triplet " << N_trackhypotheses1;
    LOG(DEBUG) << "Number of possible hypotheses for the second triplet " << N_trackhypotheses2;


    //compute  all combinations of points for both sides to form Ntlets
    map<string, Cluster*> cluster_memory;     //helper map to store the current set of clusters for the Ntlet hypothesis computation

    //...first side
    vector<Track*>track_hypotheses1;
    product(0, 0, N1, detectors, clusters, cluster_memory, track_hypotheses1);

    //...second side
    vector<Track*>track_hypotheses2;
    product(N1, N1, N2, detectors, clusters, cluster_memory, track_hypotheses2);

    //sort all hypotheses by their track chi2/ndf
    sort( track_hypotheses1.begin( ), track_hypotheses1.end( ), compareTrackChi2NDof);
    sort( track_hypotheses2.begin( ), track_hypotheses2.end( ), compareTrackChi2NDof);

    LOG(DEBUG) << "Number of track hypotheses for the first triplet " << track_hypotheses1.size();
    LOG(DEBUG) << "Number of track hypotheses for the second triplet " << track_hypotheses2.size();


    //reduce the number of hypotheses
    Tracks* tracks1 = new Tracks();
    for (size_t i = 0; i < track_hypotheses1.size(); i++) {
        if (track_hypotheses1[i]->chi2ndof() > chi2PerNDOFCut1) break;      //limit by chi2/ndfo
        if (i >= NTracks1Max) break;                                        //limit total number
        tracks1->push_back(track_hypotheses1[i]);
    }

    Tracks* tracks2 = new Tracks();
    for (size_t j = 0; j < track_hypotheses2.size(); j++) {
        if (track_hypotheses2[j]->chi2ndof() > chi2PerNDOFCut2) break;      //limit by chi2/ndfo
        if (j >= NTracks2Max) break;                                        //limit total number
        tracks2->push_back(track_hypotheses2[j]);
    }

    LOG(DEBUG) << "Number of chi2-selected tracks in the first Ntlet: " << tracks1->size();
    LOG(DEBUG) << "Number of chi2-selected tracks in the second Ntlet: " << tracks2->size();



    /*******      *******/
    //Summary up to here:
    //-treated both sides independently
    //-computed all possible connections of clusters and fitted straight line tracks
    //-only good ones + a limited number of these Ntlets were accepted

    //in the following: find out which Ntlets belong together
    /*******      *******/



    /*******   Step 3: find good pairs of Ntlets   *******/
    vector<pair<int, int> > goodNtletPairs;
    //good pairs must:
    //- point to the same location on the reference plane
    //- have a consistent direction of flight

    for (size_t i = 0; i < tracks1->size(); i++) for (size_t j = 0; j < tracks2->size(); j++) { //loop over all possible pairs

            //dX, dY: difference between pointing of the two Ntlets at the reference plane placed at zExtrapolate
            double dX = tracks1->at(i)->intercept(zExtrapolate).X() - tracks2->at(j)->intercept(zExtrapolate).X();
            double dY = tracks1->at(i)->intercept(zExtrapolate).Y() - tracks2->at(j)->intercept(zExtrapolate).Y();
            //due to misalignment of the planes, both dX do not necessarily peak at 0 but might have an offset referred t as centerX_ref/centerY_ref here
            double distance = sqrt(pow(dX - centerX_ref, 2) + pow(dY - centerY_ref, 2));

            //kink angle between both Ntlets computed from scalar product
            _kinkAngle = acos((tracks1->at(i)->m_direction.X() * tracks2->at(j)->m_direction.X() + tracks1->at(i)->m_direction.Y() * tracks2->at(j)->m_direction.Y() + tracks1->at(i)->m_direction.Z() * tracks2->at(j)->m_direction.Z()) / sqrt((tracks1->at(i)->m_direction.mag2() * tracks2->at(j)->m_direction.mag2())));

            //rejection based on the pointing distance, maxDistance is configurable
            if (distance > maxDistance) continue;

            if (performReferenceAnalysis) {     //the cut on the kink angle would bias the reference analysis which targets at mapping of kink angle distribution to the impact positions from the two Ntlets
                _impactDUT1X = (tracks1->at(i)->intercept(zExtrapolate).X() + tracks2->at(j)->intercept(zExtrapolate).X()) / 2;
                _impactDUT1Y = (tracks1->at(i)->intercept(zExtrapolate).Y() + tracks2->at(j)->intercept(zExtrapolate).Y()) / 2;
                _kinkAngleX = atan(tracks1->at(i)->m_direction.X()) - atan(tracks2->at(j)->m_direction.X());
                _kinkAngleY = atan(tracks1->at(i)->m_direction.Y()) - atan(tracks2->at(j)->m_direction.Y());

                kinkAngleTree->Fill();
                referenceHitMap->Fill(_impactDUT1X, _impactDUT1Y);
                referenceHitMapMeanAngularWeighted->Fill(_impactDUT1X, _impactDUT1Y, _kinkAngle);
                kinkAngle->Fill(_kinkAngle);
            }


            //rejection based on the kink angle, maxKinkAngle is configurable
            if (_kinkAngle > maxKinkAngle) continue;

            //if distance and kink angle cuts do not apply, a good pair is found
            goodNtletPairs.push_back(make_pair(i, j));
            displacementReference->Fill(dX, dY);
            displacementReference_dXvsX->Fill(tracks1->at(i)->intercept(zExtrapolate).X(), dX);
            displacementReference_dYvsY->Fill(tracks1->at(i)->intercept(zExtrapolate).Y(), dY);
            displacementReference_dYvsX->Fill(tracks1->at(i)->intercept(zExtrapolate).X(), dY);
            displacementReference_dXvsY->Fill(tracks1->at(i)->intercept(zExtrapolate).Y(), dX);

        }

    /*******   Step 4: refit to make a full track including all points from the detectors   *******/
    //disclaimer (05th July 2018): Ideally one would use a model that includes the multiple scattering at the reference position (e.g. GBL). This would be included as soon as the corresponding dependencies are provided on CERN's centrally provided software installations.
    Tracks* full_tracks = new Tracks();
    for (size_t f = 0; f < goodNtletPairs.size(); f++) {
        Clusters clusters1 = tracks1->at(goodNtletPairs[f].first)->clusters();
        Clusters clusters2 = tracks2->at(goodNtletPairs[f].second)->clusters();
        Track* fullTrack = new Track();
        for (size_t cl1 = 0; cl1 < clusters1.size(); cl1++) fullTrack->addCluster(clusters1[cl1]);
        for (size_t cl2 = 0; cl2 < clusters2.size(); cl2++) fullTrack->addCluster(clusters2[cl2]);
        fullTrack->fit();
        //as the straight line model at the reference position is not consistent with the multiple scattering at the reference position, track quality measures from the two Ntlets are assigned to the full track.
        fullTrack->m_chi2 = tracks1->at(goodNtletPairs[f].first)->chi2() + tracks2->at(goodNtletPairs[f].second)->chi2();
        fullTrack->m_chi2ndof = tracks1->at(goodNtletPairs[f].first)->chi2ndof() + tracks2->at(goodNtletPairs[f].second)->chi2ndof();
        fullTrack->m_ndof = tracks1->at(goodNtletPairs[f].first)->ndof() + tracks2->at(goodNtletPairs[f].second)->ndof();

        full_tracks->push_back(fullTrack);
    }


    /*******   Step 5: sort by chi2 and add the full tracks to the clipboard   *******/
    //sort by chi2
    sort( full_tracks->begin( ), full_tracks->end( ), compareTrackChi2NDof);
    //add to the event
    if (full_tracks->size() > 0) {
        clipboard->put("tracks", (Objects*)full_tracks);
    }


    /*******   Step 6: book-keeping of track properties (i.e. number of, chi2s, angles and residuals for each track type), useful for possible prealignment   *******/
    //first set of Ntlets
    tracks1PerEvent->Fill(tracks1->size());
    for (size_t i = 0; i < tracks1->size(); i++) {
        track1Chi2ndof->Fill(tracks1->at(i)->chi2ndof());
        track1Angle->Fill(atan(tracks1->at(i)->m_direction.X()), atan(tracks1->at(i)->m_direction.Y()));
        Clusters trackClusters = tracks1->at(i)->clusters();
        for (auto& trackCluster : trackClusters) {
            string detectorID = trackCluster->detectorID();
            ROOT::Math::XYZPoint intercept = tracks1->at(i)->intercept(trackCluster->globalZ());
            residualsNtletX[detectorID]->Fill(intercept.X() - trackCluster->globalX());
            residualsNtletY[detectorID]->Fill(intercept.Y() - trackCluster->globalY());

            residualsNtletX_vs_X[detectorID]->Fill(intercept.X(), intercept.X() - trackCluster->globalX());
            residualsNtletX_vs_Y[detectorID]->Fill(intercept.Y(), intercept.X() - trackCluster->globalX());
            residualsNtletY_vs_X[detectorID]->Fill(intercept.X(), intercept.Y() - trackCluster->globalY());
            residualsNtletY_vs_Y[detectorID]->Fill(intercept.Y(), intercept.Y() - trackCluster->globalY());

            residualsNtlet[detectorID]->Fill(intercept.X() - trackCluster->globalX(), intercept.Y() - trackCluster->globalY());
        }
    }
    //second set of Ntlets
    tracks2PerEvent->Fill(tracks2->size());
    for (size_t i = 0; i < tracks2->size(); i++) {
        track2Chi2ndof->Fill(tracks2->at(i)->chi2ndof());
        track2Angle->Fill(atan(tracks2->at(i)->m_direction.X()), atan(tracks2->at(i)->m_direction.Y()));
        Clusters trackClusters = tracks2->at(i)->clusters();
        for (auto& trackCluster : trackClusters) {
            string detectorID = trackCluster->detectorID();
            ROOT::Math::XYZPoint intercept = tracks2->at(i)->intercept(trackCluster->globalZ());
            residualsNtletX[detectorID]->Fill(intercept.X() - trackCluster->globalX());
            residualsNtletY[detectorID]->Fill(intercept.Y() - trackCluster->globalY());

            residualsNtletX_vs_X[detectorID]->Fill(intercept.X(), intercept.X() - trackCluster->globalX());
            residualsNtletX_vs_Y[detectorID]->Fill(intercept.Y(), intercept.X() - trackCluster->globalX());
            residualsNtletY_vs_X[detectorID]->Fill(intercept.X(), intercept.Y() - trackCluster->globalY());
            residualsNtletY_vs_Y[detectorID]->Fill(intercept.Y(), intercept.Y() - trackCluster->globalY());

            residualsNtlet[detectorID]->Fill(intercept.X() - trackCluster->globalX(), intercept.Y() - trackCluster->globalY());
        }
    }
    //full tracks
    tracksPerEvent->Fill(full_tracks->size());
    for (size_t i = 0; i < full_tracks->size(); i++) {
        trackChi2ndof->Fill(full_tracks->at(i)->chi2ndof());
        trackAngle->Fill(atan(full_tracks->at(i)->m_direction.X()), atan(full_tracks->at(i)->m_direction.Y()));
        Clusters trackClusters = full_tracks->at(i)->clusters();
        for (auto& trackCluster : trackClusters) {
            string detectorID = trackCluster->detectorID();
            ROOT::Math::XYZPoint intercept = full_tracks->at(i)->intercept(trackCluster->globalZ());
            residualsX[detectorID]->Fill(intercept.X() - trackCluster->globalX());
            residualsY[detectorID]->Fill(intercept.Y() - trackCluster->globalY());
            residuals[detectorID]->Fill(intercept.X() - trackCluster->globalX(), intercept.Y() - trackCluster->globalY());

            residualsX_vs_X[detectorID]->Fill(intercept.X(), intercept.X() - trackCluster->globalX());
            residualsX_vs_Y[detectorID]->Fill(intercept.Y(), intercept.X() - trackCluster->globalX());
            residualsY_vs_X[detectorID]->Fill(intercept.X(), intercept.Y() - trackCluster->globalY());
            residualsY_vs_Y[detectorID]->Fill(intercept.Y(), intercept.Y() - trackCluster->globalY());
        }
    }

    /*******   Last: cleanup   *******/
    for (std::vector< Track* >::iterator it = track_hypotheses1.begin() ; it != track_hypotheses1.end(); ++it) delete (*it); track_hypotheses1.clear();
    for (std::vector< Track* >::iterator it = track_hypotheses2.begin() ; it != track_hypotheses2.end(); ++it) delete (*it); track_hypotheses2.clear();


    LOG(DEBUG) << "End of event";
    return Success;
}

void NtletTracking::finalise() {
    /*******  Mean kink angle as a function of the impact position    *******/
    if (performReferenceAnalysis) referenceHitMapMeanAngularWeighted->Divide(referenceHitMap);

    /*******      *******/
    //Prealignment step 1:
    //Rotates both arms independetly such that the kink angles in x and y of both sets of Ntlets peaks at zero after its application.
    //This corresponds to using the beam line as a reference to orient the detector planes.
    /*******      *******/
    if (prealignAnglesWithBeamLine) {
        LOG(INFO) << "Performing prealignment based on track angles...";

        double angle1X, angle1Y, angle2X, angle2Y;
        //determine mean angles of track 1/2
        TF1* gaus = new TF1("gaus", "gaus+[4]");
        TH1D* angleX_Ntlet1 = track1Angle->ProjectionX();
        TH1D* angleY_Ntlet1 = track1Angle->ProjectionY();
        TH1D* angleX_Ntlet2 = track2Angle->ProjectionX();
        TH1D* angleY_Ntlet2 = track2Angle->ProjectionY();
        LOG(DEBUG) << "Gaussian fit for angleX of Ntlet 1";
        gaus->SetParameter(1, angleX_Ntlet1->GetMean());
        gaus->SetParameter(2, angleX_Ntlet1->GetRMS());
        gaus->SetRange(angleX_Ntlet1->GetMean() - 1.0 * angleX_Ntlet1->GetRMS(), angleX_Ntlet1->GetMean() + 1.0 * angleX_Ntlet1->GetRMS());
        angleX_Ntlet1->Fit(gaus, "RQ");
        angle1X = gaus->GetParameter(1);
        LOG(DEBUG) << "Gaussian fit for angleY of Ntlet 1";
        gaus->SetParameter(1, angleY_Ntlet1->GetMean());
        gaus->SetParameter(2, angleY_Ntlet1->GetRMS());
        gaus->SetRange(angleY_Ntlet1->GetMean() - 1.0 * angleY_Ntlet1->GetRMS(), angleY_Ntlet1->GetMean() + 1.0 * angleY_Ntlet1->GetRMS());
        angleY_Ntlet1->Fit(gaus, "RQ");
        angle1Y = gaus->GetParameter(1);
        LOG(DEBUG) << "Gaussian fit for angleX of Ntlet 2";
        gaus->SetParameter(1, angleX_Ntlet2->GetMean());
        gaus->SetParameter(2, angleX_Ntlet2->GetRMS());
        gaus->SetRange(angleX_Ntlet2->GetMean() - 1.0 * angleX_Ntlet2->GetRMS(), angleX_Ntlet2->GetMean() + 1.0 * angleX_Ntlet2->GetRMS());
        angleX_Ntlet2->Fit(gaus, "RQ");
        angle2X = gaus->GetParameter(1);
        LOG(DEBUG) << "Gaussian fit for angleY of Ntlet 2";
        gaus->SetParameter(1, angleY_Ntlet2->GetMean());
        gaus->SetParameter(2, angleY_Ntlet2->GetRMS());
        gaus->SetRange(angleY_Ntlet2->GetMean() - 1.0 * angleY_Ntlet2->GetRMS(), angleY_Ntlet2->GetMean() + 1.0 * angleY_Ntlet2->GetRMS());
        angleY_Ntlet2->Fit(gaus, "RQ");
        angle2Y = gaus->GetParameter(1);
        LOG(INFO) << "angle1X: " << angle1X;
        LOG(INFO) << "angle1Y: " << angle1Y;
        LOG(INFO) << "angle2X: " << angle2X;
        LOG(INFO) << "angle2Y: " << angle2Y;

        double z_ref = 0;
        for (int d = 0; d < N1; d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);
            if (d == 0) {
                z_ref = detector->displacement().Z();
            } else {
                double dZ = detector->displacement().Z() - z_ref;
                double x0 = detector->displacement().X();
                double y0 = detector->displacement().Y();
                detector->displacementX(x0 - dZ * tan(angle1X));
                detector->displacementY(y0 - dZ * tan(angle1Y));
            }
            detector->rotationX(-angle1X);
            detector->rotationY(-angle1Y);
        }

        for (int d = N1; d < N2; d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);
            if (d == N1) {
                z_ref = detector->displacement().Z();
            } else {
                double dZ = detector->displacement().Z() - z_ref;
                double x0 = detector->displacement().X();
                double y0 = detector->displacement().Y();
                detector->displacementX(x0 - dZ * tan(angle2X));
                detector->displacementY(y0 - dZ * tan(angle2Y));
            }
            detector->rotationX(-angle2X);
            detector->rotationY(-angle2Y);
        }
        delete gaus;
    }

    /*******      *******/
    //Prealignment step 2:
    //Fixes the first arm and translates the second arm such that the mean difference in the positioning at the reference plane is zero.
    /*******      *******/
    if (prealignTranslationWithBeamLine) {
        LOG(INFO) << "Performing prealignment based on Ntlet track XY intercept at z = " << zExtrapolate << "mm";
        //determine mean displacement of the tracks at extrapolation plane
        TF1* gaus = new TF1("gaus", "gaus+[4]");
        double displacementX, displacementY;
        //todo: determine mean angles of track 1/2
        TH1D* displacementX_h = displacementReference->ProjectionX();
        TH1D* displacementY_h = displacementReference->ProjectionY();
        LOG(DEBUG) << "Gaussian fit for displacement in X";
        gaus->SetParameter(1, displacementX_h->GetMean());
        gaus->SetParameter(2, displacementX_h->GetRMS());
        gaus->SetRange(displacementX_h->GetMean() - 1.0 * displacementX_h->GetRMS(), displacementX_h->GetMean() + 1.0 * displacementX_h->GetRMS());
        displacementX_h->Fit(gaus, "RQ");
        displacementX = gaus->GetParameter(1);
        LOG(DEBUG) << "Gaussian fit for displacement in X";
        gaus->SetParameter(1, displacementY_h->GetMean());
        gaus->SetParameter(2, displacementY_h->GetRMS());
        gaus->SetRange(displacementY_h->GetMean() - 1.0 * displacementY_h->GetRMS(), displacementY_h->GetMean() + 1.0 * displacementY_h->GetRMS());
        displacementY_h->Fit(gaus, "RQ");
        displacementY = gaus->GetParameter(1);
        LOG(INFO) << "displacementX: " << displacementX;
        LOG(INFO) << "displacementY: " << displacementY;


        for (int d = N1; d < N2; d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);

            //displacement is define asd extrapolation from NTlet 1 - extrapolation from NTlet 2 --> must add the displacement to Ntlet 2
            double x0 = detector->displacement().X();
            double y0 = detector->displacement().Y();
            detector->displacementX(x0 + displacementX);
            detector->displacementY(y0 + displacementY);

        }
        delete gaus;
    }

    /*******      *******/
    //Prealignment step 3:
    //Uses full tracks' residuals for the translation of planes.
    /*******      *******/
    if (prealignTranslationWithResiduals) {
        LOG(INFO) << "Performing prealignment based on full track residuals";
        //determine mean residuals
        TF1* gaus = new TF1("gaus", "gaus");

        for (int d = 0; d < N2; d++) {
            std::string detector_name = detectors[d];
            Detector* detector = get_detector(detector_name);

            double mean_x = residualsX[detector_name]->GetMean();
            double rms_x = residualsX[detector_name]->GetRMS();
            double mean_y = residualsY[detector_name]->GetMean();
            double rms_y = residualsY[detector_name]->GetRMS();
            gaus->SetParameter(1, mean_x);
            gaus->SetParameter(2, rms_x);
            gaus->SetRange(mean_x - 1.0 * rms_x, mean_x + 1.0 * rms_x);
            residualsX[detector_name]->Fit(gaus, "RQ");
            double displacementX = gaus->GetParameter(1);
            gaus->SetParameter(1, mean_y);
            gaus->SetParameter(2, rms_y);
            gaus->SetRange(mean_y - 1.0 * rms_y, mean_y + 1.0 * rms_y);
            residualsY[detector_name]->Fit(gaus, "RQ");
            double displacementY = gaus->GetParameter(1);
            LOG(INFO) << "Detector: " << detector_name;
            LOG(INFO) << "displacementX: " << displacementX;
            LOG(INFO) << "displacementY: " << displacementY;

            double x0 = detector->displacement().X();
            double y0 = detector->displacement().Y();
            detector->displacementX(x0 + displacementX);
            detector->displacementY(y0 + displacementY);
        }
        delete gaus;
    }

    /*******      *******/
    //Prealignment step 4:
    //Uses displacement of triplet tracks at the reference as a function of the impact itself to infer a rotation.
    /*******      *******/
    if (prealignRotationWithDisplacement) {
        for (auto& detector : get_detectors()) {
            string detectorID = detector->name();

            TF1* lin_fit_dY_X = new TF1("lin_fit_dY_X", "pol1", -5.0, 5.0);     //hard coded values for the DATURA telescope, DESY March 2018
            TF1* lin_fit_dX_Y = new TF1("lin_fit_dX_Y", "pol1", -2.5, 2.5);     //hard coded values for the DATURA telescope, DESY March 2018

            TProfile* profile_dY_X = residualsY_vs_X[detectorID]->ProfileX();
            TProfile* profile_dX_Y = residualsX_vs_Y[detectorID]->ProfileX();

            profile_dY_X->Fit(lin_fit_dY_X, "R");
            profile_dX_Y->Fit(lin_fit_dX_Y, "R");

            double rot1_arm2 = -asin(lin_fit_dY_X->GetParameter(1));
            double rot2_arm2 = asin(lin_fit_dX_Y->GetParameter(1));

            LOG(INFO) << "Rotations for detector " << detectorID << ": " << rot1_arm2 << ", " << rot2_arm2;

            detector->rotationZ(-(rot1_arm2 + rot2_arm2) / 2.);


            delete lin_fit_dY_X;
            delete lin_fit_dX_Y;
        }
    }


}
